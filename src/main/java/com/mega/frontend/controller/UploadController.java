package com.mega.frontend.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mega.frontend.service.UploadService;
import com.mega.frontend.utils.UtilBase64Image;

@Controller
public class UploadController {
	
	@Autowired
	private UploadService uploadService;
	
	@GetMapping("/upload_first")
	public String uploadLeasing(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		return "upload/test";
	}

	@PostMapping("/upload")
	public String singleFileUpload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes,
			Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {

		if (file.isEmpty()) {
			redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
			return "redirect:/upload_first";
		}

		try {
			File fileTemp = convert(file);
			String base64FileTemp = UtilBase64Image.encoderFile(fileTemp);
			uploadService.uploadSave("test", base64FileTemp);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return "redirect:/upload_first";
	}
	
	@GetMapping("/findByName")
	public void findByName(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		String base64 = uploadService.getBase64("test");
		UtilBase64Image.decoder(base64, "E://upload//" + "test.png");
	}
	
	public static File convert(MultipartFile file) throws IOException {
	    File convFile = new File(file.getOriginalFilename());
	    convFile.createNewFile();
	    FileOutputStream fos = new FileOutputStream(convFile);
	    fos.write(file.getBytes());
	    fos.close();
	    return convFile;
	}
	
}
