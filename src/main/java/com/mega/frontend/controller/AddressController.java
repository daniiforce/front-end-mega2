package com.mega.frontend.controller;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mega.frontend.model.Address;
import com.mega.frontend.model.AddressType;
import com.mega.frontend.model.Country;
import com.mega.frontend.model.ProcessDTO;
import com.mega.frontend.model.Province;
import com.mega.frontend.service.AddressService;
import com.mega.frontend.service.AddressTypeService;
import com.mega.frontend.service.CountryService;
import com.mega.frontend.service.ProcessService;
import com.mega.frontend.service.ProvinceService;
import com.mega.frontend.utils.ConvertMultipartFile;
import com.mega.frontend.utils.DateToStringConverter;
import com.mega.frontend.utils.UtilBase64Image;

@Controller
public class AddressController {

	@Autowired
	private ProcessService service;

	@Autowired
	private AddressTypeService addressTypeService;
	
	@Autowired
	private CountryService countryService;
	
	@Autowired
	private ProvinceService provinceService;
	
	private final AddressService addressService;

	@Autowired
	public AddressController(AddressService addressService) {
		this.addressService = addressService;
	}
	
	@GetMapping("/view_process_address")
	public String listProcess(Model model) {
		Map<String, Object> mapProcess = service.listProcess();
		List<Map<String, Object>> listProcess = (List<Map<String, Object>>) mapProcess.get("processes");
		Map<String, Object> getProcess = listProcess.get(0);

		List<ProcessDTO> processDtos = new ArrayList<ProcessDTO>();
		
		ProcessDTO processDto = new ProcessDTO();
		processDto.setId(getProcess.get("process-id").toString());
		processDto.setContainer(getProcess.get("container-id").toString());
		processDto.setName(getProcess.get("process-name").toString());
		processDto.setVersion(getProcess.get("process-version").toString());
		processDto.setPackageProcess(getProcess.get("package").toString());
		
		processDtos.add(processDto);
		
		model.addAttribute("list", processDtos);
		
		return "list";
	}
	
	@GetMapping("/findAllAddress")
	public String findAll(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		ArrayList<Address> addresses = (ArrayList<Address>) addressService.findAllByEmpId(username);
		
		List<Address> addressesTemp = new ArrayList<>();
		ListIterator<Address> ls = addresses.listIterator();
		while (ls.hasNext()) {
			ObjectMapper objectMapper = new ObjectMapper();
			Address address = objectMapper.convertValue(ls.next(), Address.class);
			
			Date dateStartDate = null;
			Date dateEndDate = null;
			try {
				dateStartDate = new SimpleDateFormat("yyyyMMdd").parse(address.getStartDate());
				dateEndDate = new SimpleDateFormat("yyyyMMdd").parse(address.getEndDate());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			address.setStartDateTemp(dateStartDate);
			address.setEndDateTemp(dateEndDate);
			
			Province province = provinceService.getByCode(address.getProvince());
			address.setProvinceStr(province.getDescription());
			
			AddressType addressType = addressTypeService.getByCode(address.getAddressType());
			address.setAddressTypeStr(addressType.getDescription());
			
			Country country = countryService.getByCountryId(address.getCountry());
			address.setCountryStr(country.getName());
			
			addressesTemp.add(address);
		}
		model.addAttribute("addresses", addressesTemp);
		
		return "address/list";
	}
	
	@RequestMapping("/edit_address")
	public String editAddress(Model model, @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, 
			@RequestParam("empId") String empId, @RequestParam("addressType") String addressType, @RequestParam("seq") String seq,
			final HttpSession session, final HttpServletRequest request, final HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		
		Address address = addressService.findByAllId(empId, startDate, endDate, addressType, seq);
		
		if(address != null) {
			address.setRequestorId(address.getEmpId());
			address.setRequestorName(address.getEmpId());
			address.setEmployeeId(address.getEmpId());
			address.setEmployeeName(address.getEmpId());
			try {
				Date date1 = new SimpleDateFormat("yyyyMMdd").parse(address.getStartDate());
				Date date2 = new SimpleDateFormat("yyyyMMdd").parse(address.getEndDate());
				address.setStartDateTemp(date1);
				address.setEndDateTemp(date2);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			model.addAttribute("address", address);
			model.addAttribute("addressTypes", addressTypeService.getAll());
			model.addAttribute("countries", countryService.getAll());
			model.addAttribute("provinces", provinceService.getAll());
		}
		
		return "address/add";
	}
	
	@RequestMapping("/add_address")
	public String addAddress(Model model, final HttpSession session, final HttpServletRequest request, final HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		
		Address address = new Address();
		
		address.setRequestorId(username);
		address.setRequestorName(username);
		address.setEmployeeId(username);
		address.setEmployeeName(username);
		
		model.addAttribute("address", address);
		model.addAttribute("addressTypes", addressTypeService.getAll());
		model.addAttribute("countries", countryService.getAll());
		model.addAttribute("provinces", provinceService.getAll());
		
		return "address/add";
	}
	
	@GetMapping("/submit_address")
	public String submitAddress(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		Address address = addressService.findByEmpId(username);
		
		if(address != null) {
			address.setRequestorId(address.getEmpId());
			address.setRequestorName(address.getEmpId());
			address.setEmployeeId(address.getEmpId());
			address.setEmployeeName(address.getEmpId());
			try {
				Date date1 = new SimpleDateFormat("yyyyMMdd").parse(address.getStartDate());
				Date date2 = new SimpleDateFormat("yyyyMMdd").parse(address.getEndDate());
				address.setStartDateTemp(date1);
				address.setEndDateTemp(date2);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			model.addAttribute("address", address);
			model.addAttribute("addressTypes", addressTypeService.getAll());
			model.addAttribute("countries", countryService.getAll());
			model.addAttribute("provinces", provinceService.getAll());
		}else {
			model.addAttribute("address", new Address());
			model.addAttribute("addressTypes", addressTypeService.getAll());
			model.addAttribute("countries", countryService.getAll());
			model.addAttribute("provinces", provinceService.getAll());
		}
		
		return "address/add";
	}
	
	@PostMapping("/doAddAddress")
	public String doAddAddress(@RequestParam("file") MultipartFile file, Address address, Model model, final HttpSession session, final HttpServletRequest request, final HttpServletResponse response) {
			try {
				String base64FileTemp = "";
				if(file.getOriginalFilename() != null && !file.getOriginalFilename().equals("")) {
					File fileTemp = ConvertMultipartFile.convert(file);
					base64FileTemp = UtilBase64Image.encoderFile(fileTemp);
				}
				
				String username = (String) session.getAttribute("username");
				model.addAttribute("username", username);
				
				if(base64FileTemp != null && !base64FileTemp.equals("")) {
					address.setBase64(base64FileTemp);
				}

				address.setStartDate(DateToStringConverter.dateToString(address.getStartDateTemp()));
				address.setEndDate(DateToStringConverter.dateToString(address.getEndDateTemp()));
				
				address.setEmpId(username);
				address.setLandscape("100");
				address.setSeq("1");
				address.setCreated_by(username);
				address.setCreated_date(DateToStringConverter.dateToString(new Date()));
				address.setLast_change(DateToStringConverter.dateToString(new Date()));
				address.setUser_change(username);

				int statusCode = addressService.submitAddress("ess-kjar-1.MasterDataUpdate", address, username);
				if(statusCode == 201) {
					model.addAttribute("info", "Address is successfully submitted.");
					model.addAttribute("address", new Address());
					return "address/add";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "redirect:/submit_address";
	}
	
	@GetMapping("/cancel_add_address")
	public String cancelAddAddress() {
		return "redirect:/findAllAddress";
	}
	
	@PostMapping("/doSaveAddress")
	public String doSaveAddress(Address address, Model model, final HttpSession session, final HttpServletRequest request, final HttpServletResponse response) {
			try {
				String username = (String) session.getAttribute("username");
				model.addAttribute("username", username);

				addressService.saveAddress(address);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "redirect:/submit_address2";
	}
	
	@GetMapping("/submit_address2")
	public String submitAddress2(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		
		Address address = addressService.findByEmpId(username);
		model.addAttribute("address", address);
		return "address/add2";
	}
}