package com.mega.frontend.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mega.frontend.model.Address;
import com.mega.frontend.model.Family;
import com.mega.frontend.model.Reward;
import com.mega.frontend.model.TasksDTO;
import com.mega.frontend.model.WorkingExperience;
import com.mega.frontend.service.AddressService;
import com.mega.frontend.service.AddressTypeService;
import com.mega.frontend.service.CountryService;
import com.mega.frontend.service.DataTypeService;
import com.mega.frontend.service.FamilyService;
import com.mega.frontend.service.FamilyTypeService;
import com.mega.frontend.service.GenderService;
import com.mega.frontend.service.IndustryService;
import com.mega.frontend.service.LastEducationService;
import com.mega.frontend.service.ProcessService;
import com.mega.frontend.service.ProvinceService;
import com.mega.frontend.service.RewardService;
import com.mega.frontend.service.TasksService;
import com.mega.frontend.service.WorkingExperienceService;
import com.mega.frontend.utils.DateToStringConverter;

@Controller
public class TasksController {
	
	@Autowired
	private ProcessService processService;
	
	@Autowired
	private AddressService addressService;
	
	@Autowired
	private AddressTypeService addressTypeService;
	
	@Autowired
	private CountryService countryService;
	
	@Autowired
	private ProvinceService provinceService;
	
	@Autowired
	private DataTypeService dataTypeService;
	
	@Autowired
	private RewardService rewardService;
	
	@Autowired
	private IndustryService industryService;
	
	@Autowired
	private WorkingExperienceService workingExperienceService;
	
	@Autowired
	private FamilyService familyService;
	
	@Autowired
	private FamilyTypeService familyTypeService;
	
	@Autowired
	private GenderService genderService;

	@Autowired
	private LastEducationService lastEducationService;

	private final TasksService service;
	
	@Autowired
	public TasksController(TasksService service) {
		this.service = service;
	}
	
	@GetMapping("/tasks_owners")
	public String TasksOwners(Model model, final HttpSession session, final HttpServletRequest request,
			final HttpServletResponse response) {
		String username = (String) session.getAttribute("username");

		model.addAttribute("username", username);
		
		Map<String, Object> mapTasks = service.ownerTasks(username, "B@nkMega1");
		
		List<Map<String, Object>> listTasks = (List<Map<String, Object>>) mapTasks.get("task-summary");
		
		List<TasksDTO> listTasksDTOs = new ArrayList<>();
		if(!listTasks.isEmpty()) {
			for(int i=0;i<listTasks.size();i++) {
				TasksDTO tasksDTO = new TasksDTO();
				Map<String, Object> getTasks = listTasks.get(i);

				tasksDTO.setTaskActualOwner(getTasks.get("task-actual-owner").toString());
				tasksDTO.setTaskContainerId(getTasks.get("task-container-id").toString());
				tasksDTO.setTaskCreatedBy(getTasks.get("task-created-by").toString());
				tasksDTO.setTaskId(getTasks.get("task-id").toString());
				tasksDTO.setTaskName(getTasks.get("task-name").toString());
				tasksDTO.setTaskProcDefId(getTasks.get("task-proc-def-id").toString());
				tasksDTO.setTaskProcInstId(getTasks.get("task-proc-inst-id").toString());
				tasksDTO.setTaskStatus(getTasks.get("task-status").toString());
				
				Map<String, Object> mapTaskCreatedOn = (Map<String, Object>) getTasks.get("task-created-on");
				tasksDTO.setTaskCreatedOn(new Date(Long.valueOf(mapTaskCreatedOn.get("java.util.Date").toString())));
				
				Map<String, Object> mapWorkItems = processService.listWorkItems(Integer.valueOf(getTasks.get("task-proc-inst-id").toString()));
				List<Map<String, Object>> listWorkItems = (List<Map<String, Object>>) mapWorkItems.get("work-item-instance");
				if(!listWorkItems.isEmpty()) {
					Map<String, Object> mapWorkItemInstance = listWorkItems.get(0);
					Map<String, Object> mapWorkItemParams = (Map<String, Object>) mapWorkItemInstance.get("work-item-params");
					if(mapWorkItemParams.get("reward") != null) {
						Map<String, Object> mapReward = (Map<String, Object>) mapWorkItemParams.get("reward");
						tasksDTO.setModelAtWorkItems(mapReward);
						tasksDTO.setTempInfo("Reward");
					}else if(mapWorkItemParams.get("address") != null) {
						Map<String, Object> mapAddress = (Map<String, Object>) mapWorkItemParams.get("address");
						tasksDTO.setModelAtWorkItems(mapAddress);
						tasksDTO.setTempInfo("Address");
					}else if(mapWorkItemParams.get("workExperience") != null) {
						Map<String, Object> mapWorkingExperience = (Map<String, Object>) mapWorkItemParams.get("workExperience");
						tasksDTO.setModelAtWorkItems(mapWorkingExperience);
						tasksDTO.setTempInfo("Working Experience");
					}else if(mapWorkItemParams.get("family") != null) {
						Map<String, Object> mapFamily = (Map<String, Object>) mapWorkItemParams.get("family");
						tasksDTO.setModelAtWorkItems(mapFamily);
						tasksDTO.setTempInfo("Family");
					}
				}
				listTasksDTOs.add(tasksDTO);
			}
		}
		model.addAttribute("tasks", listTasksDTOs);
		
		return "tasks/list";
	}
	
	@RequestMapping("/approve_tasks")
	public String approveTasks(Model model, @RequestParam("id") String taskId, final HttpSession session, final HttpServletRequest request,
			final HttpServletResponse response) {
		String username = (String) session.getAttribute("username");

		Map<String, Object> getTasks = service.getTask(username, "B@nkMega1", taskId);
		TasksDTO tasksDTO = new TasksDTO();

		tasksDTO.setTaskActualOwner(getTasks.get("task-actual-owner").toString());
		tasksDTO.setTaskContainerId(getTasks.get("task-container-id").toString());
		tasksDTO.setTaskCreatedBy(getTasks.get("task-created-by").toString());
		tasksDTO.setTaskId(getTasks.get("task-id").toString());
		tasksDTO.setTaskName(getTasks.get("task-name").toString());
		tasksDTO.setTaskProcInstId(getTasks.get("task-process-instance-id").toString());
		tasksDTO.setTaskStatus(getTasks.get("task-status").toString());
		tasksDTO.setTaskWorkItemId(getTasks.get("task-workitem-id").toString());
		
		Map<String, Object> mapWorkItems = processService.listWorkItems(Integer.valueOf(getTasks.get("task-process-instance-id").toString()));
		List<Map<String, Object>> listWorkItems = (List<Map<String, Object>>) mapWorkItems.get("work-item-instance");
		if(!listWorkItems.isEmpty()) {
			Map<String, Object> mapWorkItemInstance = listWorkItems.get(0);
			Map<String, Object> mapWorkItemParams = (Map<String, Object>) mapWorkItemInstance.get("work-item-params");
			if(mapWorkItemParams.get("reward") != null) {
				Map<String, Object> mapReward = (Map<String, Object>) mapWorkItemParams.get("reward");
				Reward reward = new Reward();
				
				reward.setAmount(mapReward.get("amount") != null ? mapReward.get("amount").toString() : "");
				reward.setBase64(mapReward.get("base64") != null ? mapReward.get("base64").toString() : "");
				reward.setComment(mapReward.get("comment") != null ? mapReward.get("comment").toString() : "");
				reward.setCreated_by(mapReward.get("created_by") != null ? mapReward.get("created_by").toString() : "");
				reward.setCreated_date(mapReward.get("created_date") != null ? mapReward.get("created_date").toString() : "");
				reward.setData_type(mapReward.get("data_type") != null ? mapReward.get("data_type").toString() : "");
				reward.setDescription(mapReward.get("description") != null ? mapReward.get("description").toString() : "");
				reward.setEffective_date(mapReward.get("effective_date") != null ? mapReward.get("effective_date").toString() : "");
				reward.setEmpId(mapReward.get("empId") != null ? mapReward.get("empId").toString() : "");
				reward.setEmployeeId(mapReward.get("employeeId") != null ? mapReward.get("employeeId").toString() : "");
				reward.setEmployeeName(mapReward.get("employeeName") != null ? mapReward.get("employeeName").toString() : "");
				reward.setEndDate(mapReward.get("endDate") != null ? mapReward.get("endDate").toString() : "");
				reward.setLandscape(mapReward.get("landscape") != null ? mapReward.get("landscape").toString() : "");
				reward.setLast_change(mapReward.get("last_change") != null ? mapReward.get("last_change").toString() : "");
				reward.setNo_sk(mapReward.get("no_sk") != null ? mapReward.get("no_sk").toString() : "");
				reward.setRequestorId(mapReward.get("requestorId") != null ? mapReward.get("requestorId").toString() : "");
				reward.setRequestorName(mapReward.get("requestorName") != null ? mapReward.get("requestorName").toString() : "");
				reward.setResponsible(mapReward.get("responsible") != null ? mapReward.get("responsible").toString() : "");
				reward.setSeq(mapReward.get("seq") != null ? mapReward.get("seq").toString() : "");
				reward.setStartDate(mapReward.get("startDate") != null ? mapReward.get("startDate").toString() : "");
				reward.setSubject(mapReward.get("subject") != null ? mapReward.get("subject").toString() : "");
				reward.setUser_change(mapReward.get("user_change") != null ? mapReward.get("user_change").toString() : "");
				reward.setDescriptionTemp(mapReward.get("descriptionTemp") != null ? mapReward.get("descriptionTemp").toString() : "");
				
				try {
					Date date1 = new SimpleDateFormat("yyyyMMdd").parse(reward.getStartDate());
					Date date2 = new SimpleDateFormat("yyyyMMdd").parse(reward.getEndDate());
					
					reward.setStartDateTemp(date1);
					reward.setEndDateTemp(date2);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				tasksDTO.setReward(reward);
				
				String prefixBase64Img = "data:image/png;base64,";
				model.addAttribute("attach", prefixBase64Img + reward.getBase64());
				model.addAttribute("dataTypes", dataTypeService.getAll());
			}else if(mapWorkItemParams.get("address") != null) {
				Map<String, Object> mapAddress = (Map<String, Object>) mapWorkItemParams.get("address");
				Address address = new Address();
				
				address.setAddressType(mapAddress.get("addressType") != null ? mapAddress.get("addressType").toString() : "");
				address.setCellphone_num(mapAddress.get("cellphone_num") !=  null ? mapAddress.get("cellphone_num").toString() : "");
				address.setCity(mapAddress.get("city") != null ? mapAddress.get("city").toString() : "");
				address.setContact_person(mapAddress.get("contact_person") != null ? mapAddress.get("contact_person").toString() : "");
				address.setCountry(mapAddress.get("country") != null ? mapAddress.get("country").toString() : "");
				address.setCreated_by(mapAddress.get("created_by") != null ? mapAddress.get("created_by").toString() : "");
				address.setCreated_date(mapAddress.get("created_date") != null ? mapAddress.get("created_date").toString() : "");
				address.setEmpId(mapAddress.get("empId") != null ? mapAddress.get("empId").toString() : "");
				address.setEndDate(mapAddress.get("endDate") != null ? mapAddress.get("endDate").toString() : "");
				address.setKecamatan(mapAddress.get("kecamatan") != null ? mapAddress.get("kecamatan").toString() : "" );
				address.setKelurahan(mapAddress.get("kelurahan") != null ? mapAddress.get("kelurahan").toString() : "");
				address.setKepemilikan(mapAddress.get("kepemilikan") != null ? mapAddress.get("kepemilikan").toString() : "");
				address.setLandscape(mapAddress.get("landscape") != null ? mapAddress.get("landscape").toString() : "");
				address.setLast_change(mapAddress.get("last_change") != null ? mapAddress.get("last_change").toString() : "");
				address.setLocation(mapAddress.get("location") != null ? mapAddress.get("location").toString() : "");
				address.setPostalcode(mapAddress.get("postalcode") != null ? mapAddress.get("postalcode").toString() : "");
				address.setProvince(mapAddress.get("province") != null ? mapAddress.get("province").toString() : "");
				address.setRt(mapAddress.get("rt") != null ? mapAddress.get("rt").toString() : "");
				address.setRw(mapAddress.get("rw") != null ? mapAddress.get("rw").toString() : "");
				address.setSeq(mapAddress.get("seq") != null ? mapAddress.get("seq").toString() : "");
				address.setStartDate(mapAddress.get("startDate") != null ? mapAddress.get("startDate").toString() : "");
				address.setStreet(mapAddress.get("street") != null ? mapAddress.get("street").toString() : "");
				address.setTelp_num(mapAddress.get("telp_num") != null ? mapAddress.get("telp_num").toString() : "");
				address.setUser_change(mapAddress.get("user_change") != null ? mapAddress.get("user_change").toString() : "");
				address.setSubject(mapAddress.get("subject") != null ? mapAddress.get("subject").toString() : "");
				address.setDescription(mapAddress.get("description") != null ? mapAddress.get("description").toString() : "");
				address.setComment(mapAddress.get("comment") != null ? mapAddress.get("comment").toString() : "");
				address.setBase64(mapAddress.get("base64") != null ? mapAddress.get("base64").toString() : "");
				address.setRequestorId(mapAddress.get("requestorId") != null ? mapAddress.get("requestorId").toString() : "");
				address.setRequestorName(mapAddress.get("requestorName") != null ? mapAddress.get("requestorName").toString() : "");
				address.setEmployeeId(mapAddress.get("employeeId") != null ? mapAddress.get("employeeId").toString() : "");
				address.setEmployeeName(mapAddress.get("employeeName") != null ? mapAddress.get("employeeName").toString() : "");

				try {
					Date date1 = new SimpleDateFormat("yyyyMMdd").parse(address.getStartDate());
					Date date2 = new SimpleDateFormat("yyyyMMdd").parse(address.getEndDate());
					
					address.setStartDateTemp(date1);
					address.setEndDateTemp(date2);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				tasksDTO.setAddress(address);
				
				String prefixBase64Img = "data:image/png;base64,";
				model.addAttribute("attach", prefixBase64Img + address.getBase64());
				model.addAttribute("addressTypes", addressTypeService.getAll());
				model.addAttribute("countries", countryService.getAll());
				model.addAttribute("provinces", provinceService.getAll());
			}else if(mapWorkItemParams.get("workExperience") != null) {
				Map<String, Object> mapWorkingExperience = (Map<String, Object>) mapWorkItemParams.get("workExperience");
				WorkingExperience workingExperience = new WorkingExperience();
				
				workingExperience.setBase64(mapWorkingExperience.get("base64") != null ? mapWorkingExperience.get("base64").toString() : "");
				workingExperience.setComment(mapWorkingExperience.get("comment") != null ? mapWorkingExperience.get("comment").toString() : "");
				workingExperience.setCompany(mapWorkingExperience.get("company") != null ? mapWorkingExperience.get("company").toString() : "");
				workingExperience.setCreated_by(mapWorkingExperience.get("created_by") != null ? mapWorkingExperience.get("created_by").toString() : "");
				workingExperience.setCreated_date(mapWorkingExperience.get("created_date") != null ? mapWorkingExperience.get("created_date").toString() : "");
				workingExperience.setDescription(mapWorkingExperience.get("description") != null ? mapWorkingExperience.get("description").toString() : "");
				workingExperience.setDescriptionTemp(mapWorkingExperience.get("descriptionTemp") != null ? mapWorkingExperience.get("descriptionTemp").toString() : "");
				workingExperience.setDoc_completion(mapWorkingExperience.get("doc_completion") != null ? mapWorkingExperience.get("doc_completion").toString() : "");
				workingExperience.setEmpId(mapWorkingExperience.get("empId") != null ? mapWorkingExperience.get("empId").toString() : "");
				workingExperience.setEmployeeId(mapWorkingExperience.get("employeeId") != null ? mapWorkingExperience.get("employeeId").toString() : "");
				workingExperience.setEmployeeName(mapWorkingExperience.get("employeeName") != null ? mapWorkingExperience.get("employeeName").toString() : "");
				workingExperience.setEndDate(mapWorkingExperience.get("endDate") != null ? mapWorkingExperience.get("endDate").toString() : "");
				workingExperience.setIkatan_dinas(mapWorkingExperience.get("ikatan_dinas") != null ? mapWorkingExperience.get("ikatan_dinas").toString() : "");
				workingExperience.setIndustry(mapWorkingExperience.get("industry") != null ? mapWorkingExperience.get("industry").toString() : "");
				workingExperience.setJob(mapWorkingExperience.get("job") != null ? mapWorkingExperience.get("job").toString() : "");
				workingExperience.setKompensasi_terakhir(mapWorkingExperience.get("kompensasi_terakhir") != null ? mapWorkingExperience.get("kompensasi_terakhir").toString() : "");
				workingExperience.setLandscape(mapWorkingExperience.get("landscape") != null ? mapWorkingExperience.get("landscape").toString() : "");
				workingExperience.setLast_change(mapWorkingExperience.get("last_change") != null ? mapWorkingExperience.get("last_change").toString() : "");
				workingExperience.setLocation(mapWorkingExperience.get("location") != null ? mapWorkingExperience.get("location").toString() : "");
				workingExperience.setPhone_number(mapWorkingExperience.get("phone_number") != null ? mapWorkingExperience.get("phone_number").toString() : "");
				workingExperience.setPosition(mapWorkingExperience.get("position") != null ? mapWorkingExperience.get("position").toString() : "");
				workingExperience.setReason_leaving(mapWorkingExperience.get("reason_leaving") != null ? mapWorkingExperience.get("reason_leaving").toString() : "");
				workingExperience.setRequestorId(mapWorkingExperience.get("requestorId") != null ? mapWorkingExperience.get("requestorId").toString() : "");
				workingExperience.setRequestorName(mapWorkingExperience.get("requestorName") != null ? mapWorkingExperience.get("requestorName").toString() : "");
				workingExperience.setSeq(mapWorkingExperience.get("seq") != null ? mapWorkingExperience.get("seq").toString() : "");
				workingExperience.setStartDate(mapWorkingExperience.get("startDate") != null ? mapWorkingExperience.get("startDate").toString() : "");
				workingExperience.setStatus_reference(mapWorkingExperience.get("status_reference") != null ? mapWorkingExperience.get("status_reference").toString() : "");
				workingExperience.setSubject(mapWorkingExperience.get("subject") != null ? mapWorkingExperience.get("subject").toString() : "");
				workingExperience.setSupervisor_name(mapWorkingExperience.get("supervisor_name") != null ? mapWorkingExperience.get("supervisor_name").toString() : "");
				workingExperience.setSupervisor_position(mapWorkingExperience.get("supervisor_position") != null ? mapWorkingExperience.get("supervisor_position").toString() : "");
				workingExperience.setUser_change(mapWorkingExperience.get("user_change") != null ? mapWorkingExperience.get("user_change").toString() : "");
				workingExperience.setWorking_exp(mapWorkingExperience.get("working_exp") != null ? mapWorkingExperience.get("working_exp").toString() : "");
				
				try {
					Date date1 = new SimpleDateFormat("yyyyMMdd").parse(workingExperience.getStartDate());
					Date date2 = new SimpleDateFormat("yyyyMMdd").parse(workingExperience.getEndDate());
					
					workingExperience.setStartDateTemp(date1);
					workingExperience.setEndDateTemp(date2);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				tasksDTO.setWorkExperience(workingExperience);
				
				String prefixBase64Img = "data:image/png;base64,";
				model.addAttribute("attach", prefixBase64Img + workingExperience.getBase64());
				model.addAttribute("industries", industryService.getAll());
			}else if(mapWorkItemParams.get("family") != null) {
				Map<String, Object> mapFamily = (Map<String, Object>) mapWorkItemParams.get("family");
				Family family = new Family();
				
				family.setAddress(mapFamily.get("address") != null ? mapFamily.get("address").toString() : "");
				family.setBase64(mapFamily.get("base64") != null ? mapFamily.get("base64").toString() : "");
				family.setBirth_date(mapFamily.get("birth_date") != null ? mapFamily.get("birth_date").toString() : "");
				family.setBirth_place(mapFamily.get("birth_place") != null ? mapFamily.get("birth_place").toString() : "");
				family.setComment(mapFamily.get("comment") != null ? mapFamily.get("comment").toString() : "");
				family.setCreated_by(mapFamily.get("created_by") != null ? mapFamily.get("created_by").toString() : "");
				family.setCreated_date(mapFamily.get("created_date") != null ? mapFamily.get("created_date").toString() : "");
				family.setDate_of_death(mapFamily.get("date_of_death") != null ? mapFamily.get("date_of_death").toString() : "");
				family.setDescriptionTemp(mapFamily.get("descriptionTemp") != null ? mapFamily.get("descriptionTemp").toString() : "");
				family.setEmpId(mapFamily.get("empId") != null ? mapFamily.get("empId").toString() : "");
				family.setEmployeeId(mapFamily.get("employeeId") != null ? mapFamily.get("employeeId").toString() : "");
				family.setEmployeeName(mapFamily.get("employeeName") != null ? mapFamily.get("employeeName").toString() : "");
				family.setEnd_date(mapFamily.get("end_date") != null ? mapFamily.get("end_date").toString() : "");
				family.setFamily_medical_code(mapFamily.get("family_medical_code") != null ? mapFamily.get("family_medical_code").toString() : "");
				family.setFamily_type(mapFamily.get("family_type") != null ? mapFamily.get("family_type").toString() : "");
				family.setGender(mapFamily.get("gender") != null ? mapFamily.get("gender").toString() : "");
				family.setLandscape(mapFamily.get("landscape") != null ? mapFamily.get("landscape").toString() : "");
				family.setLast_change(mapFamily.get("last_change") != null ? mapFamily.get("last_change").toString() : "");
				family.setLast_education(mapFamily.get("last_education") != null ? mapFamily.get("last_education").toString() : "");
				family.setMedical_flag(mapFamily.get("medical_flag") != null ? mapFamily.get("medical_flag").toString() : "");
				family.setName(mapFamily.get("name") != null ? mapFamily.get("name").toString() : "");
				family.setNo_asuransi(mapFamily.get("no_asuransi") != null ? mapFamily.get("no_asuransi").toString() : "");
				family.setNo_bpjs(mapFamily.get("no_bpjs") != null ? mapFamily.get("no_bpjs").toString() : "");
				family.setNo_cug(mapFamily.get("no_cug") != null ? mapFamily.get("no_cug").toString() : "");
				family.setNo_surat(mapFamily.get("no_surat") != null ? mapFamily.get("no_surat").toString() : "");
				family.setOccupation(mapFamily.get("occupation") != null ? mapFamily.get("occupation").toString() : "");
				family.setRequestorId(mapFamily.get("requestorId") != null ? mapFamily.get("requestorId").toString() : "");
				family.setRequestorName(mapFamily.get("requestorName") != null ? mapFamily.get("requestorName").toString() : "");
				family.setSeq(mapFamily.get("seq") != null ? mapFamily.get("seq").toString() : "");
				family.setStart_date(mapFamily.get("start_date") != null ? mapFamily.get("start_date").toString() : "");
				family.setSubject(mapFamily.get("subject") != null ? mapFamily.get("subject").toString() : "");
				family.setTelp_home(mapFamily.get("telp_home") != null ? mapFamily.get("telp_home").toString() : "");
				family.setTelp_office(mapFamily.get("telp_office") != null ? mapFamily.get("telp_office").toString() : "");
				family.setUser_change(mapFamily.get("user_change") != null ? mapFamily.get("user_change").toString() : "");
				
				try {
					Date date1 = new SimpleDateFormat("yyyyMMdd").parse(family.getStart_date());
					Date date2 = new SimpleDateFormat("yyyyMMdd").parse(family.getEnd_date());
					Date date3 = new SimpleDateFormat("yyyyMMdd").parse(family.getBirth_date());
					Date date4 = new SimpleDateFormat("yyyyMMdd").parse(family.getDate_of_death());
					
					family.setStartDateTemp(date1);
					family.setEndDateTemp(date2);
					family.setBirthDateTemp(date3);
					family.setDateOfDeathTemp(date4);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				tasksDTO.setFamily(family);
				
				String prefixBase64Img = "data:image/png;base64,";
				model.addAttribute("attach", prefixBase64Img + family.getBase64());
				model.addAttribute("familyTypes", familyTypeService.getAll());
				model.addAttribute("genders", genderService.getAll());
				model.addAttribute("lastEducations", lastEducationService.getAll());
			}
		}
		model.addAttribute("tasks", tasksDTO);
		model.addAttribute("username", username);
		
		return "tasks/approve";
	}
	
	@RequestMapping(value = "/doApproveTasks", method = RequestMethod.POST)
	public String doApproveTask(@ModelAttribute("tasks") TasksDTO tasksDTO, @RequestParam(value="action", required=true) String action, Model model, final HttpSession session, final HttpServletRequest request, final HttpServletResponse response){
		
		TasksDTO task = tasksDTO;
		String username = (String) session.getAttribute("username");
		
		if(task != null) {
			if(task.getAddress() != null) {
				task.getAddress().setStartDate(task.getAddress().getStartDateTemp() != null ? DateToStringConverter.dateToString(task.getAddress().getStartDateTemp()) : "");
				task.getAddress().setEndDate(task.getAddress().getEndDateTemp() != null ? DateToStringConverter.dateToString(task.getAddress().getEndDateTemp()) : "");
			}else if(task.getReward() != null) {
				task.getReward().setStartDate(task.getReward().getStartDateTemp() != null ? DateToStringConverter.dateToString(task.getReward().getStartDateTemp()) : "");
				task.getReward().setEndDate(task.getReward().getEndDateTemp() != null ? DateToStringConverter.dateToString(task.getReward().getEndDateTemp()) : "");
			}else if(task.getWorkExperience() != null) {
				task.getWorkExperience().setStartDate(task.getWorkExperience().getStartDateTemp() != null ? DateToStringConverter.dateToString(task.getWorkExperience().getStartDateTemp()) : "");
				task.getWorkExperience().setEndDate(task.getWorkExperience().getEndDateTemp() != null ? DateToStringConverter.dateToString(task.getWorkExperience().getEndDateTemp()) : "");
			}else if(task.getFamily() != null) {
				task.getFamily().setStart_date(task.getFamily().getStartDateTemp() != null ? DateToStringConverter.dateToString(task.getFamily().getStartDateTemp()) : "");
				task.getFamily().setEnd_date(task.getFamily().getEndDateTemp() != null ? DateToStringConverter.dateToString(task.getFamily().getEndDateTemp()) : "");
				task.getFamily().setBirth_date(task.getFamily().getBirthDateTemp() != null ? DateToStringConverter.dateToString(task.getFamily().getBirthDateTemp()) : "");
				task.getFamily().setDate_of_death(task.getFamily().getDateOfDeathTemp() != null ? DateToStringConverter.dateToString(task.getFamily().getDateOfDeathTemp()) : "");
			}

		switch(action) {
        	case "Yes":
        			int statusCode = service.startCompleteTask(username, "B@nkMega1", tasksDTO.getTaskId(), task, "Yes");
        			if(statusCode == 201) {
        				if(task.getAddress() != null) {
        					Address tempAddress = addressService.findByAllId(task.getAddress().getEmpId(), task.getAddress().getStartDate(), task.getAddress().getEndDate(), task.getAddress().getAddressType(), task.getAddress().getSeq());
        					if(tempAddress != null) {
        						addressService.editAddress(task.getAddress());
        					}else {
        						addressService.saveAddress(task.getAddress());
        					}
        				}else if(task.getReward() != null) {
        					Reward tempReward = rewardService.findByAllId(task.getReward().getEmpId(), task.getReward().getStartDate(), task.getReward().getEndDate(), task.getReward().getSeq());
        					if(tempReward != null) {
        						rewardService.editReward(task.getReward());
        					}else {
        						rewardService.saveReward(task.getReward());
        					}
        				}else if(task.getWorkExperience() != null) {
        					workingExperienceService.saveWorkingExperience(task.getWorkExperience());
        				}else if(task.getFamily() != null) {
        					familyService.saveFamily(task.getFamily());
        				}
        			}
        		break;
        	case "Reject":
        		service.startCompleteTask(username, "B@nkMega1", tasksDTO.getTaskId(), task, "Reject");
        		break;
        	case "Revise":
        		service.startCompleteTask(username, "B@nkMega1", tasksDTO.getTaskId(), task, "Revise");
        		break;
        	case "Submit":
        		service.startCompleteTask(username, "B@nkMega1", tasksDTO.getTaskId(), task, "");
        		break;
        	default:
        		break;
		}
		}
		model.addAttribute("username", username);
		return "redirect:/tasks_owners";
	}

}