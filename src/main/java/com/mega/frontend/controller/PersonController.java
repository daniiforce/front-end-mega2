package com.mega.frontend.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.mega.frontend.model.Address;
import com.mega.frontend.model.Person;
import com.mega.frontend.model.ProcessDTO;
import com.mega.frontend.service.PersonService;
import com.mega.frontend.service.ProcessService;

@Controller
public class PersonController {

	@Autowired
	private ProcessService service;

	private final PersonService personService;

	@Autowired
	public PersonController(PersonService personService) {
		this.personService = personService;
	}
	
	@GetMapping("/view_process_person")
	public String listProcess(Model model) {
		Map<String, Object> mapProcess = service.listProcess();
		List<Map<String, Object>> listProcess = (List<Map<String, Object>>) mapProcess.get("processes");
		Map<String, Object> getProcess = listProcess.get(0);

		List<ProcessDTO> processDtos = new ArrayList<ProcessDTO>();
		
		ProcessDTO processDto = new ProcessDTO();
		processDto.setId(getProcess.get("process-id").toString());
		processDto.setContainer(getProcess.get("container-id").toString());
		processDto.setName(getProcess.get("process-name").toString());
		processDto.setVersion(getProcess.get("process-version").toString());
		processDto.setPackageProcess(getProcess.get("package").toString());
		
		processDtos.add(processDto);
		
		model.addAttribute("list", processDtos);
		
		return "list";
	}
	
	@GetMapping("/submit_person")
	public String submitPerson(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		model.addAttribute("person", new Person());
		
		return "person/add";
	}
	
	@PostMapping("/doAddPerson")
	public String doAddPerson(Person person, Model model, final HttpSession session, final HttpServletRequest request, final HttpServletResponse response) {
			try {
//				Map<String, Object> mapProcess = service.listProcess();
//				List<Map<String, Object>> listProcess = (List<Map<String, Object>>) mapProcess.get("processes");
//				Map<String, Object> getProcess = listProcess.get(0);
//				
//				String processId = getProcess.get("process-id").toString();

				String username = (String) session.getAttribute("username");
				model.addAttribute("username", username);
				
				int statusCode = personService.submitPerson("ess-kjar-1.MasterDataUpdate", person, username);
				if(statusCode == 201) {
					model.addAttribute("info", "Person is successfully submitted.");
					model.addAttribute("person", new Person());
					return "person/add";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "redirect:/submit_person";
	}
}