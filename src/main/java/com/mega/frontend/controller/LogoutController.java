package com.mega.frontend.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LogoutController {

	@GetMapping("/logout")
	public String logout(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		Cookie userNameCookieRemove = new Cookie("myToken", "");
		userNameCookieRemove.setMaxAge(0);
		response.addCookie(userNameCookieRemove);

		session.invalidate();

		return "login";
	}

}
