package com.mega.frontend.controller;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mega.frontend.model.AddressType;
import com.mega.frontend.model.Country;
import com.mega.frontend.model.Province;
import com.mega.frontend.model.WorkIndustry;
import com.mega.frontend.model.WorkingExperience;
import com.mega.frontend.service.IndustryService;
import com.mega.frontend.service.WorkingExperienceService;
import com.mega.frontend.utils.ConvertMultipartFile;
import com.mega.frontend.utils.DateToStringConverter;
import com.mega.frontend.utils.UtilBase64Image;

@Controller
public class WorkingExperienceController {

	@Autowired
	private IndustryService industryService;

	private final WorkingExperienceService workingExperienceService;

	@Autowired
	public WorkingExperienceController(WorkingExperienceService workingExperienceService) {
		this.workingExperienceService = workingExperienceService;
	}
	
	@GetMapping("/submit_working_experience")
	public String submitWorkingExperience(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		WorkingExperience workingExperience = workingExperienceService.findByEmpId(username);
		
		if(workingExperience != null) {
			workingExperience.setRequestorId(workingExperience.getEmpId());
			workingExperience.setRequestorName(workingExperience.getEmpId());
			workingExperience.setEmployeeId(workingExperience.getEmpId());
			workingExperience.setEmployeeName(workingExperience.getEmpId());
			try {
				Date date1 = new SimpleDateFormat("yyyyMMdd").parse(workingExperience.getStartDate());
				Date date2 = new SimpleDateFormat("yyyyMMdd").parse(workingExperience.getEndDate());
				workingExperience.setStartDateTemp(date1);
				workingExperience.setEndDateTemp(date2);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			model.addAttribute("workingExperience", workingExperience);
			model.addAttribute("industries", industryService.getAll());
		}else {
			WorkingExperience workingExperienceTemp = new WorkingExperience();
			
			workingExperienceTemp.setRequestorId(username);
			workingExperienceTemp.setRequestorName(username);
			workingExperienceTemp.setEmployeeId(username);
			workingExperienceTemp.setEmployeeName(username);
			
			workingExperienceTemp.setLandscape("100");
			workingExperienceTemp.setEmpId(username);
			workingExperienceTemp.setSeq("1");
			
			workingExperienceTemp.setUser_change(username);
			workingExperienceTemp.setCreated_by(username);
			
			DateFormat dateFormat = new SimpleDateFormat("yyyymmdd");
			workingExperienceTemp.setLast_change(dateFormat.format(new Date()));
			workingExperienceTemp.setCreated_date(dateFormat.format(new Date()));

			model.addAttribute("workingExperience", workingExperienceTemp);
			model.addAttribute("industries", industryService.getAll());
		}
		
		return "working_experience/add";
	}
	
	@PostMapping("/doAddWorkingExperience")
	public String doAddWorkingExperience(@RequestParam("file") MultipartFile file, WorkingExperience workingExperience, Model model, final HttpSession session, final HttpServletRequest request, final HttpServletResponse response) {
			try {
				String base64FileTemp = "";
				if(file.getOriginalFilename() != null && !file.getOriginalFilename().equals("")) {
					File fileTemp = ConvertMultipartFile.convert(file);
					base64FileTemp = UtilBase64Image.encoderFile(fileTemp);
				}
				
				String username = (String) session.getAttribute("username");
				model.addAttribute("username", username);
				
				if(base64FileTemp != null && !base64FileTemp.equals("")) {
					workingExperience.setBase64(base64FileTemp);
				}
				
				workingExperience.setStartDate(DateToStringConverter.dateToString(workingExperience.getStartDateTemp()));
				workingExperience.setEndDate(DateToStringConverter.dateToString(workingExperience.getEndDateTemp()));
				
				workingExperience.setEmpId(username);
				workingExperience.setLandscape("100");
				workingExperience.setSeq("1");
				workingExperience.setCreated_by(username);
				workingExperience.setCreated_date(DateToStringConverter.dateToString(new Date()));
				workingExperience.setLast_change(DateToStringConverter.dateToString(new Date()));
				workingExperience.setUser_change(username);

				int statusCode = workingExperienceService.submitWorkingExperience("ess-kjar-1.MasterDataUpdate", workingExperience, username);
				if(statusCode == 201) {
					model.addAttribute("info", "Working Experience is successfully submitted.");
					model.addAttribute("workingExperience", new WorkingExperience());
					return "working_experience/add";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "redirect:/submit_working_experience";
	}
	
	@RequestMapping("/add_working_experience")
	public String addWorkingExperience(Model model, final HttpSession session, final HttpServletRequest request, final HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		
		WorkingExperience workingExperience = new WorkingExperience();
		
		workingExperience.setRequestorId(username);
		workingExperience.setRequestorName(username);
		workingExperience.setEmployeeId(username);
		workingExperience.setEmployeeName(username);
		
		model.addAttribute("workingExperience", workingExperience);
		model.addAttribute("industries", industryService.getAll());
		
		return "working_experience/add";
	}
	
	@GetMapping("/cancel_add_working_experience")
	public String cancelAddWorkingExperience() {
		return "redirect:/findAllWorkingExperience";
	}
	
	@RequestMapping("/edit_working_experience")
	public String editWorkingExperience(Model model, @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, 
			@RequestParam("empId") String empId, @RequestParam("seq") String seq,
			final HttpSession session, final HttpServletRequest request, final HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		
		WorkingExperience workingExperience = workingExperienceService.findByAllId(empId, startDate, endDate, seq);
		
		if(workingExperience != null) {
			workingExperience.setRequestorId(workingExperience.getEmpId());
			workingExperience.setRequestorName(workingExperience.getEmpId());
			workingExperience.setEmployeeId(workingExperience.getEmpId());
			workingExperience.setEmployeeName(workingExperience.getEmpId());
			try {
				Date date1 = new SimpleDateFormat("yyyyMMdd").parse(workingExperience.getStartDate());
				Date date2 = new SimpleDateFormat("yyyyMMdd").parse(workingExperience.getEndDate());
				workingExperience.setStartDateTemp(date1);
				workingExperience.setEndDateTemp(date2);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			model.addAttribute("workingExperience", workingExperience);
			model.addAttribute("industries", industryService.getAll());
		}
		
		return "working_experience/add";
	}
	
	@GetMapping("/findAllWorkingExperience")
	public String findAllWorkingExperience(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		ArrayList<WorkingExperience> workingExperiences = (ArrayList<WorkingExperience>) workingExperienceService.findAllByEmpId(username);
		
		List<WorkingExperience> workingExperiencesTemp = new ArrayList<>();
		ListIterator<WorkingExperience> ls = workingExperiences.listIterator();
		while (ls.hasNext()) {
			ObjectMapper objectMapper = new ObjectMapper();
			WorkingExperience workingExperience = objectMapper.convertValue(ls.next(), WorkingExperience.class);
			
			Date dateStartDate = null;
			Date dateEndDate = null;
			try {
				dateStartDate = new SimpleDateFormat("yyyyMMdd").parse(workingExperience.getStartDate());
				dateEndDate = new SimpleDateFormat("yyyyMMdd").parse(workingExperience.getEndDate());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			workingExperience.setStartDateTemp(dateStartDate);
			workingExperience.setEndDateTemp(dateEndDate);
			
			WorkIndustry workIndustry = industryService.getByCode(workingExperience.getIndustry());
			workingExperience.setIndustryStr(workIndustry.getDescription());
			
			workingExperiencesTemp.add(workingExperience);
		}
		model.addAttribute("workingExperiences", workingExperiencesTemp);
		
		return "working_experience/list";
	}
	
}