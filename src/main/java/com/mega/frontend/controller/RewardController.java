package com.mega.frontend.controller;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mega.frontend.model.DataType;
import com.mega.frontend.model.Reward;
import com.mega.frontend.service.DataTypeService;
import com.mega.frontend.service.RewardService;
import com.mega.frontend.utils.ConvertMultipartFile;
import com.mega.frontend.utils.DateToStringConverter;
import com.mega.frontend.utils.UtilBase64Image;

@Controller
public class RewardController {

	@Autowired
	private DataTypeService dataTypeService;

	private final RewardService rewardService;

	@Autowired
	public RewardController(RewardService rewardService) {
		this.rewardService = rewardService;
	}
	
	@GetMapping("/submit_reward")
	public String submitReward(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		Reward reward = rewardService.findByEmpId(username);
		
		if(reward != null) {
			reward.setRequestorId(reward.getEmpId());
			reward.setRequestorName(reward.getEmpId());
			reward.setEmployeeId(reward.getEmpId());
			reward.setEmployeeName(reward.getEmpId());
			try {
				Date date1 = new SimpleDateFormat("yyyyMMdd").parse(reward.getStartDate());
				Date date2 = new SimpleDateFormat("yyyyMMdd").parse(reward.getEndDate());
				reward.setStartDateTemp(date1);
				reward.setEndDateTemp(date2);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			model.addAttribute("reward", reward);
			model.addAttribute("dataTypes", dataTypeService.getAll());
		}else {
			Reward rewardTemp = new Reward();
			
			rewardTemp.setRequestorId(username);
			rewardTemp.setRequestorName(username);
			rewardTemp.setEmployeeId(username);
			rewardTemp.setEmployeeName(username);
			
			rewardTemp.setLandscape("100");
			rewardTemp.setEmpId(username);
			rewardTemp.setSeq("1");
			
			rewardTemp.setUser_change(username);
			rewardTemp.setCreated_by(username);
			
			DateFormat dateFormat = new SimpleDateFormat("yyyymmdd");
			rewardTemp.setLast_change(dateFormat.format(new Date()));
			rewardTemp.setCreated_date(dateFormat.format(new Date()));

			model.addAttribute("reward", rewardTemp);
			model.addAttribute("dataTypes", dataTypeService.getAll());
		}
		
		return "reward/add";
	}
	
	@PostMapping("/doAddReward")
	public String doAddReward(@RequestParam("file") MultipartFile file, Reward reward, Model model, final HttpSession session, final HttpServletRequest request, final HttpServletResponse response) {
			try {
				String base64FileTemp = "";
				if(file.getOriginalFilename() != null && !file.getOriginalFilename().equals("")) {
					File fileTemp = ConvertMultipartFile.convert(file);
					base64FileTemp = UtilBase64Image.encoderFile(fileTemp);
				}
				
				String username = (String) session.getAttribute("username");
				model.addAttribute("username", username);
				
				if(base64FileTemp != null && !base64FileTemp.equals("")) {
					reward.setBase64(base64FileTemp);
				}
				
				reward.setStartDate(DateToStringConverter.dateToString(reward.getStartDateTemp()));
				reward.setEndDate(DateToStringConverter.dateToString(reward.getEndDateTemp()));

				reward.setEmpId(username);
				reward.setLandscape("100");
				reward.setSeq("1");
				reward.setCreated_by(username);
				reward.setCreated_date(DateToStringConverter.dateToString(new Date()));
				reward.setLast_change(DateToStringConverter.dateToString(new Date()));
				reward.setUser_change(username);
				
				int statusCode = rewardService.submitReward("ess-kjar-1.MasterDataUpdate", reward, username);
				if(statusCode == 201) {
					model.addAttribute("info", "Reward is successfully submitted.");
					model.addAttribute("reward", new Reward());
					return "reward/add";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "redirect:/submit_reward";
	}
	
	@GetMapping("/findAllReward")
	public String findAllReward(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		ArrayList<Reward> rewards = (ArrayList<Reward>) rewardService.findAllByEmpId(username);
		
		List<Reward> rewardsTemp = new ArrayList<>();
		ListIterator<Reward> ls = rewards.listIterator();
		while (ls.hasNext()) {
			ObjectMapper objectMapper = new ObjectMapper();
			Reward reward = objectMapper.convertValue(ls.next(), Reward.class);
			
			Date dateStartDate = null;
			Date dateEndDate = null;
			try {
				dateStartDate = new SimpleDateFormat("yyyyMMdd").parse(reward.getStartDate());
				dateEndDate = new SimpleDateFormat("yyyyMMdd").parse(reward.getEndDate());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			reward.setStartDateTemp(dateStartDate);
			reward.setEndDateTemp(dateEndDate);
			
			DataType dataType = dataTypeService.getByCode(reward.getData_type());
			reward.setDataTypeStr(dataType.getDescription());
			
			rewardsTemp.add(reward);
		}
		model.addAttribute("rewards", rewardsTemp);
		
		return "reward/list";
	}
	
	@RequestMapping("/add_reward")
	public String addReward(Model model, final HttpSession session, final HttpServletRequest request, final HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		
		Reward reward = new Reward();
		
		reward.setRequestorId(username);
		reward.setRequestorName(username);
		reward.setEmployeeId(username);
		reward.setEmployeeName(username);
		
		model.addAttribute("reward", reward);
		model.addAttribute("dataTypes", dataTypeService.getAll());
		
		return "reward/add";
	}
	
	@RequestMapping("/edit_reward")
	public String editReward(Model model, @RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, 
			@RequestParam("empId") String empId, @RequestParam("seq") String seq,
			final HttpSession session, final HttpServletRequest request, final HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		
		Reward reward = rewardService.findByAllId(empId, startDate, endDate, seq);
		
		if(reward != null) {
			reward.setRequestorId(reward.getEmpId());
			reward.setRequestorName(reward.getEmpId());
			reward.setEmployeeId(reward.getEmpId());
			reward.setEmployeeName(reward.getEmpId());
			try {
				Date date1 = new SimpleDateFormat("yyyyMMdd").parse(reward.getStartDate());
				Date date2 = new SimpleDateFormat("yyyyMMdd").parse(reward.getEndDate());
				reward.setStartDateTemp(date1);
				reward.setEndDateTemp(date2);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			model.addAttribute("reward", reward);
			model.addAttribute("dataTypes", dataTypeService.getAll());
		}
		
		return "reward/add";
	}


}