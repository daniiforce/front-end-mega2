package com.mega.frontend.controller;

import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.mega.frontend.model.LoginDTO;
import com.mega.frontend.service.LoginService;

@Controller
public class LoginController {

	private final LoginService service;
	
	@Autowired
	public LoginController(LoginService service) {
		this.service = service;
	}

	@GetMapping("login")
	public String getAll() {
		return "login";
	}
	
	@GetMapping("dashboard")
	public String getLayout(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		
		return "dashboard/index";
	}

	@PostMapping("loginProcess")
	public String loginProcess(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("login") LoginDTO login) {
		LoginDTO loginDto = new LoginDTO();
		loginDto = login;
		Map<String, Object> token = service.login(loginDto);
		String passingToken = (String) token.get("token");
		
		if(passingToken.equalsIgnoreCase("")) {
			return "redirect:/login";
		}else {
			myServiceMethodSettingCookie(request, response, passingToken);
//			Map<String, Object> user = (Map<String, Object>) token.get("user");
//			String username = user.get("username").toString();
			
			String username = token.get("user").toString();
			session.setAttribute("username", username);
		}
		return "redirect:/dashboard";
	}
	
	private void myServiceMethodSettingCookie(HttpServletRequest request, HttpServletResponse response, String token) {
		final String cookieName = "myToken";
		final String cookieValue = token; // you could assign it some encoded value
		final Boolean useSecureCookie = false;
		final int expiryTime = 60 * 60 * 24; // 24h in seconds
		final String cookiePath = "/";

		Cookie cookie = new Cookie(cookieName, cookieValue);

		cookie.setSecure(useSecureCookie); // determines whether the cookie should only be sent using a secure protocol,
											// such as HTTPS or SSL

		cookie.setMaxAge(expiryTime); // A negative value means that the cookie is not stored persistently and will be
										// deleted when the Web browser exits. A zero value causes the cookie to be
										// deleted.

		cookie.setPath(cookiePath); // The cookie is visible to all the pages in the directory you specify, and all
									// the pages in that directory's subdirectories

		response.addCookie(cookie);
	}
}