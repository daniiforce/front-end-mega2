package com.mega.frontend.controller;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.mega.frontend.model.Family;
import com.mega.frontend.service.FamilyTypeService;
import com.mega.frontend.service.GenderService;
import com.mega.frontend.service.LastEducationService;
import com.mega.frontend.service.FamilyService;
import com.mega.frontend.utils.ConvertMultipartFile;
import com.mega.frontend.utils.UtilBase64Image;

@Controller
public class FamilyController {

	@Autowired
	private FamilyTypeService familyTypeService;
	
	@Autowired
	private GenderService genderService;

	@Autowired
	private LastEducationService lastEducationService;

	private final FamilyService familyService;

	@Autowired
	public FamilyController(FamilyService familyService) {
		this.familyService = familyService;
	}
	
	@GetMapping("/submit_family")
	public String submitFamily(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		String username = session.getAttribute("username").toString();
		model.addAttribute("username", username);
		Family family = familyService.findByEmpId(username);
		
		if(family != null) {
			family.setRequestorId(family.getEmpId());
			family.setRequestorName(family.getEmpId());
			family.setEmployeeId(family.getEmpId());
			family.setEmployeeName(family.getEmpId());
			
			try {
				Date date1 = new SimpleDateFormat("yyyyMMdd").parse(family.getStart_date());
				Date date2 = new SimpleDateFormat("yyyyMMdd").parse(family.getEnd_date());
				family.setStartDateTemp(date1);
				family.setEndDateTemp(date2);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			model.addAttribute("family", family);
			model.addAttribute("familyTypes", familyTypeService.getAll());
			model.addAttribute("genders", genderService.getAll());
			model.addAttribute("lastEducations", lastEducationService.getAll());
		}else {
			Family familyTemp = new Family();
			
			familyTemp.setRequestorId(username);
			familyTemp.setRequestorName(username);
			familyTemp.setEmployeeId(username);
			familyTemp.setEmployeeName(username);
			
			familyTemp.setLandscape("100");
			familyTemp.setEmpId(username);
			familyTemp.setSeq("1");
			
			familyTemp.setUser_change(username);
			familyTemp.setCreated_by(username);
			
			DateFormat dateFormat = new SimpleDateFormat("yyyymmdd");
			familyTemp.setLast_change(dateFormat.format(new Date()));
			familyTemp.setCreated_date(dateFormat.format(new Date()));

			model.addAttribute("family", familyTemp);
			model.addAttribute("familyTypes", familyTypeService.getAll());
			model.addAttribute("genders", genderService.getAll());
			model.addAttribute("lastEducations", lastEducationService.getAll());
		}
		
		return "family/add";
	}
	
	@PostMapping("/doAddFamily")
	public String doAddFamily(@RequestParam("file") MultipartFile file, Family family, Model model, final HttpSession session, final HttpServletRequest request, final HttpServletResponse response) {
			try {
				File fileTemp = ConvertMultipartFile.convert(file);
				String base64FileTemp = UtilBase64Image.encoderFile(fileTemp);
				
				String username = (String) session.getAttribute("username");
				model.addAttribute("username", username);
				
				DateFormat dateFormat = new SimpleDateFormat("yyyymmdd");
				
				family.setBase64(base64FileTemp != null ? base64FileTemp : "");
				if(family.getStartDateTemp() != null) {
					family.setStart_date(dateFormat.format(family.getStartDateTemp()));
				}
				if(family.getEndDateTemp() != null) {
					family.setEnd_date(dateFormat.format(family.getEndDateTemp()));
				}
				if(family.getDateOfDeathTemp() != null) {
					family.setDate_of_death(dateFormat.format(family.getDateOfDeathTemp()));
				}
				if(family.getBirthDateTemp() != null) {
					family.setBirth_date(dateFormat.format(family.getBirthDateTemp()));
				}

				int statusCode = familyService.submitFamily("ess-kjar-1.MasterDataUpdate", family, username);
				if(statusCode == 201) {
					model.addAttribute("info", "Family is successfully submitted.");
					model.addAttribute("family", new Family());
					return "family/add";
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return "redirect:/submit_family";
	}
	
}