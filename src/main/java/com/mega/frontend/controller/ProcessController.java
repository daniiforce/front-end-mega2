package com.mega.frontend.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.mega.frontend.model.ProcessDTO;
import com.mega.frontend.service.ProcessService;

@Controller
public class ProcessController {

	private final ProcessService service;
	
	@Autowired
	public ProcessController(ProcessService service) {
		this.service = service;
	}
	
	@GetMapping("/view_process")
	public String listProcess(Model model) {
		Map<String, Object> mapProcess = service.listProcess();
		List<Map<String, Object>> listProcess = (List<Map<String, Object>>) mapProcess.get("processes");
		Map<String, Object> getProcess = listProcess.get(0);

		List<ProcessDTO> processDtos = new ArrayList<ProcessDTO>();
		
		ProcessDTO processDto = new ProcessDTO();
		processDto.setId(getProcess.get("process-id").toString());
		processDto.setContainer(getProcess.get("container-id").toString());
		processDto.setName(getProcess.get("process-name").toString());
		processDto.setVersion(getProcess.get("process-version").toString());
		processDto.setPackageProcess(getProcess.get("package").toString());
		
		processDtos.add(processDto);
		
		model.addAttribute("list", processDtos);
		
		return "list";
	}
}