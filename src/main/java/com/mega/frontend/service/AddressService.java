package com.mega.frontend.service;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.Address;

@Service
public class AddressService {

	private final RestTemplate restTemplate;

	@Autowired
	public AddressService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${address.submitAddress}")
	private String ADDRESS_SUBMIT;
	
	@Value("${address.save}")
	private String SAVE_ADDRESS;

	@Value("${address.findByEmpId}")
	private String ADDRESS_FIND_BY_EMP_ID;

	@Value("${address.edit}")
	private String EDIT_ADDRESS;
	
	@Value("${address.findAll}")
	private String ADDRESS_FIND_ALL;
	
	@Value("${address.findByAllId}")
	private String ADDRESS_FIND_BY_ALL_ID;
	
	@Value("${address.findAllByEmpId}")
	private String ADDRESS_FIND_BY_ALL_EMP_ID;

	public int submitAddress(String process_id, Address address, String username) {
		
		LinkedHashMap<String, Object> data = new LinkedHashMap<>();
		data.put("process_id", process_id);
		data.put("address", address);
		data.put("username", username);
		
		HttpEntity httpEntity = new HttpEntity<>(data);

		ResponseEntity<Integer> i = restTemplate.exchange(HOST_AND_PORT + ADDRESS_SUBMIT, HttpMethod.POST, httpEntity, Integer.class);
		int h = i.getBody();
		return h;
	}
	
	public void saveAddress(Address address) {
		
		HttpEntity httpEntity = new HttpEntity<>(address);

		restTemplate.exchange(HOST_AND_PORT + SAVE_ADDRESS, HttpMethod.POST, httpEntity, Address.class);
	}
	
	public Address findByEmpId(String empId) {
		ResponseEntity<?> address = restTemplate.getForEntity(HOST_AND_PORT + ADDRESS_FIND_BY_EMP_ID + empId, Address.class);
		Address address2 = (Address) address.getBody();
		return address2;
	}
	
	public Address findByAllId(String empId, String startDate, String endDate, String addressType, String seq) {
		LinkedHashMap<String, Object> data = new LinkedHashMap<>();
		data.put("empId", empId);
		data.put("startDate", startDate);
		data.put("endDate", endDate);
		data.put("addressType", addressType);
		data.put("seq", seq);

		HttpEntity httpEntity = new HttpEntity<>(data);
		
		ResponseEntity<?> address = restTemplate.exchange(HOST_AND_PORT + ADDRESS_FIND_BY_ALL_ID, HttpMethod.POST, 
				httpEntity, Address.class);
		
		Address address2 = (Address) address.getBody();
		return address2;
	}
	
	public List<Address> findAll() {
		ResponseEntity<?> address = restTemplate.getForEntity(HOST_AND_PORT + ADDRESS_FIND_ALL, List.class);
		List<Address> address2 = (List<Address>) address.getBody();
		return address2;
	}
	
	public List<Address> findAllByEmpId(String empId) {
		ResponseEntity<?> address = restTemplate.getForEntity(HOST_AND_PORT + ADDRESS_FIND_BY_ALL_EMP_ID + empId, List.class);
		List<Address> address2 = (List<Address>) address.getBody();
		return address2;
	}
	
	public void editAddress(Address address) {
		HttpEntity httpEntity = new HttpEntity<>(address);
		restTemplate.exchange(HOST_AND_PORT + EDIT_ADDRESS + address.getEmpId(), HttpMethod.POST, httpEntity, Address.class);
	}
	
}
