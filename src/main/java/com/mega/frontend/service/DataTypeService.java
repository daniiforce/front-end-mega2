package com.mega.frontend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.DataType;

@Service
public class DataTypeService {

	private final RestTemplate restTemplate;

	@Autowired
	public DataTypeService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${data_type.findall}")
	private String DATA_TYPE_FIND_ALL;
	
	@Value("${data_type.findByCode}")
	private String DATA_TYPE_FIND_BY_CODE;

	public List<DataType> getAll() {
		ResponseEntity<?> dataType = restTemplate.getForEntity(HOST_AND_PORT + DATA_TYPE_FIND_ALL, List.class);
		List<DataType> dataTypes = (List<DataType>) dataType.getBody();
		return dataTypes;
	}
	
	public DataType getByCode(String code) {
		ResponseEntity<?> dataType = restTemplate.getForEntity(HOST_AND_PORT + DATA_TYPE_FIND_BY_CODE + code, DataType.class);
		DataType dataTypes = (DataType) dataType.getBody();
		return dataTypes;
	}

}
