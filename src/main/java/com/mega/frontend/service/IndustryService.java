package com.mega.frontend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.WorkIndustry;

@Service
public class IndustryService {

	private final RestTemplate restTemplate;

	@Autowired
	public IndustryService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${industry.findall}")
	private String INDUSTRY_FIND_ALL;
	
	@Value("${industry.findByCode}")
	private String INDUSTRY_FIND_BY_CODE;

	public List<WorkIndustry> getAll() {
		ResponseEntity<?> industry = restTemplate.getForEntity(HOST_AND_PORT + INDUSTRY_FIND_ALL, List.class);
		List<WorkIndustry> industries = (List<WorkIndustry>) industry.getBody();
		return industries;
	}
	
	public WorkIndustry getByCode(String code) {
		ResponseEntity<?> workIndustry = restTemplate.getForEntity(HOST_AND_PORT + INDUSTRY_FIND_BY_CODE + code, WorkIndustry.class);
		WorkIndustry workIndustrys = (WorkIndustry) workIndustry.getBody();
		return workIndustrys;
	}

}
