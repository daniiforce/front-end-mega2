package com.mega.frontend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.FamilyType;

@Service
public class FamilyTypeService {

	private final RestTemplate restTemplate;

	@Autowired
	public FamilyTypeService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${family_type.findall}")
	private String FAMILY_TYPE_FIND_ALL;

	public List<FamilyType> getAll() {
		ResponseEntity<?> familyType = restTemplate.getForEntity(HOST_AND_PORT + FAMILY_TYPE_FIND_ALL, List.class);
		List<FamilyType> familyTypes = (List<FamilyType>) familyType.getBody();
		return familyTypes;
	}

}
