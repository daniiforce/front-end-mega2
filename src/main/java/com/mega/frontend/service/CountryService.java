package com.mega.frontend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.Country;

@Service
public class CountryService {

	private final RestTemplate restTemplate;

	@Autowired
	public CountryService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${country.findall}")
	private String COUNTRY_FIND_ALL;
	
	@Value("${country.findByCode}")
	private String COUNTRY_FIND_BY_CODE;
	
	public List<Country> getAll() {
		ResponseEntity<?> country = restTemplate.getForEntity(HOST_AND_PORT + COUNTRY_FIND_ALL, List.class);
		List<Country> countries = (List<Country>) country.getBody();
		return countries;
	}
	
	public Country getByCountryId(String countryId) {
		ResponseEntity<?> country = restTemplate.getForEntity(HOST_AND_PORT + COUNTRY_FIND_BY_CODE + countryId, Country.class);
		Country countries = (Country) country.getBody();
		return countries;
	}
	
}
