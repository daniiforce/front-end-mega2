package com.mega.frontend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.Province;

@Service
public class ProvinceService {

	private final RestTemplate restTemplate;

	@Autowired
	public ProvinceService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${province.findall}")
	private String PROVINCE_FIND_ALL;
	
	@Value("${province.findByCode}")
	private String PROVINCE_FIND_BY_CODE;
	
	public List<Province> getAll() {
		ResponseEntity<?> province = restTemplate.getForEntity(HOST_AND_PORT + PROVINCE_FIND_ALL, List.class);
		List<Province> provinces = (List<Province>) province.getBody();
		return provinces;
	}
	
	public Province getByCode(String code) {
		ResponseEntity<?> province = restTemplate.getForEntity(HOST_AND_PORT + PROVINCE_FIND_BY_CODE + code, Province.class);
		Province provinces = (Province) province.getBody();
		return provinces;
	}
	
}
