package com.mega.frontend.service;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.WorkingExperience;

@Service
public class WorkingExperienceService {

	private final RestTemplate restTemplate;

	@Autowired
	public WorkingExperienceService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${working_experience.submitWorkingExperience}")
	private String WORKING_EXPERIENCE_SUBMIT;

	@Value("${working_experience.save}")
	private String SAVE_WORKING_EXPERIENCE;

	@Value("${working_experience.findByEmpId}")
	private String WORKING_EXPERIENCE_FIND_BY_EMP_ID;

	@Value("${working_experience.edit}")
	private String EDIT_WORKING_EXPERIENCE;
	
	@Value("${working_experience.findByAllId}")
	private String WORKING_EXPERIENCE_FIND_BY_ALL_ID;
	
	@Value("${working_experience.findAllByEmpId}")
	private String WORKING_EXPERIENCE_FIND_ALL_BY_EMP_ID;

	public int submitWorkingExperience(String process_id, WorkingExperience workingExperience, String username) {

		LinkedHashMap<String, Object> data = new LinkedHashMap<>();
		data.put("process_id", process_id);
		data.put("workingExperience", workingExperience);
		data.put("username", username);

		HttpEntity httpEntity = new HttpEntity<>(data);

		ResponseEntity<Integer> i = restTemplate.exchange(HOST_AND_PORT + WORKING_EXPERIENCE_SUBMIT, HttpMethod.POST, httpEntity,
				Integer.class);
		int h = i.getBody();
		return h;
	}

	public void saveWorkingExperience(WorkingExperience workingExperience) {

		HttpEntity httpEntity = new HttpEntity<>(workingExperience);

		restTemplate.exchange(HOST_AND_PORT + SAVE_WORKING_EXPERIENCE, HttpMethod.POST, httpEntity, WorkingExperience.class);
	}

	public WorkingExperience findByEmpId(String empId) {
		ResponseEntity<?> workingExperience = restTemplate.getForEntity(HOST_AND_PORT + WORKING_EXPERIENCE_FIND_BY_EMP_ID + empId,
				WorkingExperience.class);
		WorkingExperience workingExperience2 = (WorkingExperience) workingExperience.getBody();
		return workingExperience2;
	}

	public void editWorkingExperience(WorkingExperience workingExperience) {
		HttpEntity httpEntity = new HttpEntity<>(workingExperience);
		restTemplate.exchange(HOST_AND_PORT + EDIT_WORKING_EXPERIENCE + workingExperience.getEmpId(), HttpMethod.POST, httpEntity,
				WorkingExperience.class);
	}
	
	public WorkingExperience findByAllId(String empId, String startDate, String endDate, String seq) {
		LinkedHashMap<String, Object> data = new LinkedHashMap<>();
		data.put("empId", empId);
		data.put("startDate", startDate);
		data.put("endDate", endDate);
		data.put("seq", seq);

		HttpEntity httpEntity = new HttpEntity<>(data);
		
		ResponseEntity<?> workingExperience = restTemplate.exchange(HOST_AND_PORT + WORKING_EXPERIENCE_FIND_BY_ALL_ID, HttpMethod.POST, 
				httpEntity, WorkingExperience.class);
		
		WorkingExperience workingExperience2 = (WorkingExperience) workingExperience.getBody();
		return workingExperience2;
	}
	
	public List<WorkingExperience> findAllByEmpId(String empId) {
		ResponseEntity<?> workingExperience = restTemplate.getForEntity(HOST_AND_PORT + WORKING_EXPERIENCE_FIND_ALL_BY_EMP_ID + empId, List.class);
		List<WorkingExperience> workingExperience2 = (List<WorkingExperience>) workingExperience.getBody();
		return workingExperience2;
	}

}
