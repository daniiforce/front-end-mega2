package com.mega.frontend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.Gender;

@Service
public class GenderService {

	private final RestTemplate restTemplate;

	@Autowired
	public GenderService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${gender.findall}")
	private String GENDER_FIND_ALL;

	public List<Gender> getAll() {
		ResponseEntity<?> gender = restTemplate.getForEntity(HOST_AND_PORT + GENDER_FIND_ALL, List.class);
		List<Gender> genders = (List<Gender>) gender.getBody();
		return genders;
	}

}
