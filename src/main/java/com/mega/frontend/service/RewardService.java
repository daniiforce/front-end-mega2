package com.mega.frontend.service;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.Reward;

@Service
public class RewardService {

	private final RestTemplate restTemplate;

	@Autowired
	public RewardService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${reward.submitReward}")
	private String REWARD_SUBMIT;

	@Value("${reward.save}")
	private String SAVE_REWARD;

	@Value("${reward.findByEmpId}")
	private String REWARD_FIND_BY_EMP_ID;

	@Value("${reward.edit}")
	private String EDIT_REWARD;
	
	@Value("${reward.findByAllId}")
	private String REWARD_FIND_BY_ALL_ID;
	
	@Value("${reward.findAllByEmpId}")
	private String REWARD_FIND_BY_EMP_ID_LIST;

	public int submitReward(String process_id, Reward reward, String username) {

		LinkedHashMap<String, Object> data = new LinkedHashMap<>();
		data.put("process_id", process_id);
		data.put("reward", reward);
		data.put("username", username);

		HttpEntity httpEntity = new HttpEntity<>(data);

		ResponseEntity<Integer> i = restTemplate.exchange(HOST_AND_PORT + REWARD_SUBMIT, HttpMethod.POST, httpEntity,
				Integer.class);
		int h = i.getBody();
		return h;
	}

	public void saveReward(Reward reward) {

		HttpEntity httpEntity = new HttpEntity<>(reward);

		restTemplate.exchange(HOST_AND_PORT + SAVE_REWARD, HttpMethod.POST, httpEntity, Reward.class);
	}

	public Reward findByEmpId(String empId) {
		ResponseEntity<?> reward = restTemplate.getForEntity(HOST_AND_PORT + REWARD_FIND_BY_EMP_ID + empId,
				Reward.class);
		Reward reward2 = (Reward) reward.getBody();
		return reward2;
	}

	public void editReward(Reward reward) {
		HttpEntity httpEntity = new HttpEntity<>(reward);
		restTemplate.exchange(HOST_AND_PORT + EDIT_REWARD + reward.getEmpId(), HttpMethod.POST, httpEntity,
				Reward.class);
	}
	
	public Reward findByAllId(String empId, String startDate, String endDate, String seq) {
		LinkedHashMap<String, Object> data = new LinkedHashMap<>();
		data.put("empId", empId);
		data.put("startDate", startDate);
		data.put("endDate", endDate);
		data.put("seq", seq);

		HttpEntity httpEntity = new HttpEntity<>(data);
		
		ResponseEntity<?> reward = restTemplate.exchange(HOST_AND_PORT + REWARD_FIND_BY_ALL_ID, HttpMethod.POST, 
				httpEntity, Reward.class);
		
		Reward reward2 = (Reward) reward.getBody();
		return reward2;
	}
	
	public List<Reward> findAllByEmpId(String empId) {
		ResponseEntity<?> reward = restTemplate.getForEntity(HOST_AND_PORT + REWARD_FIND_BY_EMP_ID_LIST + empId, List.class);
		List<Reward> rewards = (List<Reward>) reward.getBody();
		return rewards;
	}

}
