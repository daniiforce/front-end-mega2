package com.mega.frontend.service;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.TasksDTO;
import com.mega.frontend.utils.HeaderDataSetup;

@Service
public class TasksService {

	private final RestTemplate restTemplate;

	@Autowired
	public TasksService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
/*	
	@Value("${jbpm.host.and.port}")
	private String JBPM_HOST_PORT;

	@Value("${jbpm.rest.server}")
	private String JBPM_REST_SERVER;

	@Value("${jbpm.tasks.owners}")
	private String JBPM_TASKS_OWNERS;
	
	@Value("${jbpm.get.task}")
	private String JBPM_GET_TASK;

	@Value("${jbpm.containers}")
	private String JBPM_CONTAINERS;

	public Map<String, Object> ownerTasks(String username, String password) {
		HeaderDataSetup headerDataSetup = new HeaderDataSetup(username, password);
		HttpHeaders headers = headerDataSetup.getHttpHeaders();

		HttpEntity httpEntity = new HttpEntity<>(headers);

		ResponseEntity<Map> responseTasks = restTemplate.exchange(JBPM_HOST_PORT + JBPM_REST_SERVER + JBPM_TASKS_OWNERS,
				HttpMethod.GET, httpEntity, Map.class);

		Map<String, Object> responseTask = responseTasks.getBody();
		return responseTask;
	}
	
	public Map<String, Object> getTask(String username, String password, String taskId) {
		HeaderDataSetup headerDataSetup = new HeaderDataSetup(username, password);
		HttpHeaders headers = headerDataSetup.getHttpHeaders();

		HttpEntity httpEntity = new HttpEntity<>(headers);

		ResponseEntity<Map> responseTasks = restTemplate.exchange(JBPM_HOST_PORT + JBPM_REST_SERVER + JBPM_CONTAINERS + JBPM_GET_TASK + taskId,
				HttpMethod.GET, httpEntity, Map.class);

		Map<String, Object> responseTask = responseTasks.getBody();
		return responseTask;
	}

*/
	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${tasks.ownerTasks}")
	private String TASK_OWNER;

	@Value("${tasks.getTask}")
	private String GET_TASK;

	@Value("${tasks.startCompleteTask}")
	private String START_COMPLETE_TASK;

	public Map<String, Object> ownerTasks(String username, String password) {
		LinkedHashMap<String, Object> data = new LinkedHashMap<>();
		data.put("username", username);
		data.put("password", password);
		
		HttpEntity httpEntity = new HttpEntity<>(data);

		ResponseEntity<Map> responseTasks = restTemplate.exchange(HOST_AND_PORT + TASK_OWNER,
				HttpMethod.POST, httpEntity, Map.class);

		Map<String, Object> responseTask = responseTasks.getBody();
		return responseTask;
	}
	
	public Map<String, Object> getTask(String username, String password, String taskId) {
		LinkedHashMap<String, Object> data = new LinkedHashMap<>();
		data.put("username", username);
		data.put("password", password);
		data.put("taskId", taskId);

		HttpEntity httpEntity = new HttpEntity<>(data);

		ResponseEntity<Map> responseTasks = restTemplate.exchange(HOST_AND_PORT + GET_TASK,
				HttpMethod.POST, httpEntity, Map.class);

		Map<String, Object> responseTask = responseTasks.getBody();
		return responseTask;
	}

	public int startCompleteTask(String username, String password, String taskId, TasksDTO taskDto, String approval) {
		LinkedHashMap<String, Object> data = new LinkedHashMap<>();
		data.put("username", username);
		data.put("password", password);
		data.put("taskId", taskId);
		data.put("approval", approval);

		if(taskDto.getAddress() != null) {
			data.put("address", taskDto.getAddress());
		}else if(taskDto.getReward() != null) {
			data.put("reward", taskDto.getReward());
		}else if(taskDto.getWorkExperience() != null) {
			data.put("workExperience", taskDto.getWorkExperience());
		}

		HttpEntity httpEntity = new HttpEntity<>(data);

		ResponseEntity<Integer> responseTasks = restTemplate.exchange(HOST_AND_PORT + START_COMPLETE_TASK,
				HttpMethod.POST, httpEntity, Integer.class);

		int responseTask = responseTasks.getBody();
		return responseTask;
	}

}
