package com.mega.frontend.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.LoginDTO;

@Service
public class LoginService {

	private final RestTemplate restTemplate;

	@Autowired
	public LoginService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${auth.login}")
	private String GET_TOKEN_LOGIN;

	public Map<String, Object> login(LoginDTO login) {
		Map<String, Object> token = new HashMap<>();
		try {
			token = restTemplate.postForObject(HOST_AND_PORT + GET_TOKEN_LOGIN, login, Map.class);
		}catch(Exception e) {
			e.printStackTrace();
			token.put("token", "");
			return token;
		}
		return token;
	}
	
}
