package com.mega.frontend.service;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.Family;

@Service
public class FamilyService {

	private final RestTemplate restTemplate;

	@Autowired
	public FamilyService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${family.submitFamily}")
	private String FAMILY_SUBMIT;

	@Value("${family.save}")
	private String SAVE_FAMILY;

	@Value("${family.findByEmpId}")
	private String FAMILY_FIND_BY_EMP_ID;

	@Value("${family.edit}")
	private String EDIT_FAMILY;

	public int submitFamily(String process_id, Family family, String username) {

		LinkedHashMap<String, Object> data = new LinkedHashMap<>();
		data.put("process_id", process_id);
		data.put("family", family);
		data.put("username", username);

		HttpEntity httpEntity = new HttpEntity<>(data);

		ResponseEntity<Integer> i = restTemplate.exchange(HOST_AND_PORT + FAMILY_SUBMIT, HttpMethod.POST, httpEntity,
				Integer.class);
		int h = i.getBody();
		return h;
	}

	public void saveFamily(Family family) {

		HttpEntity httpEntity = new HttpEntity<>(family);

		restTemplate.exchange(HOST_AND_PORT + SAVE_FAMILY, HttpMethod.POST, httpEntity, Family.class);
	}

	public Family findByEmpId(String empId) {
		ResponseEntity<?> family = restTemplate.getForEntity(HOST_AND_PORT + FAMILY_FIND_BY_EMP_ID + empId,
				Family.class);
		Family family2 = (Family) family.getBody();
		return family2;
	}

	public void editFamily(Family family) {
		HttpEntity httpEntity = new HttpEntity<>(family);
		restTemplate.exchange(HOST_AND_PORT + EDIT_FAMILY + family.getEmpId(), HttpMethod.POST, httpEntity,
				Family.class);
	}

}
