package com.mega.frontend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.AddressType;

@Service
public class AddressTypeService {

	private final RestTemplate restTemplate;

	@Autowired
	public AddressTypeService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${addresstype.findall}")
	private String ADDRESS_TYPE_FIND_ALL;
	
	@Value("${addresstype.findByCode}")
	private String ADDRESS_TYPE_FIND_BY_CODE;
	
	public List<AddressType> getAll() {
		ResponseEntity<?> addresstype = restTemplate.getForEntity(HOST_AND_PORT + ADDRESS_TYPE_FIND_ALL, List.class);
		List<AddressType> addresstypes = (List<AddressType>) addresstype.getBody();
		return addresstypes;
	}
	
	public AddressType getByCode(String code) {
		ResponseEntity<?> addresstype = restTemplate.getForEntity(HOST_AND_PORT + ADDRESS_TYPE_FIND_BY_CODE + code, AddressType.class);
		AddressType addresstypes = (AddressType) addresstype.getBody();
		return addresstypes;
	}
	
}
