package com.mega.frontend.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.Person;
import com.mega.frontend.utils.HeaderDataSetup;

@Service
public class PersonService {

	private final RestTemplate restTemplate;

	@Autowired
	public PersonService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
/*
	public int submitPerson(String process_id, Person person) {
		
		HeaderDataSetup headerDataSetup = new HeaderDataSetup("ORISYS02", "B@nkMega1");
		HttpHeaders headers = headerDataSetup.getHttpHeaders();

//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_JSON);
		
		Map<String, Object> mapPerson = new HashMap<>();
		mapPerson.put("person", person);
		
		HttpEntity httpEntity = new HttpEntity<>(mapPerson, headers);

		ResponseEntity<Integer> i = restTemplate.exchange(
				"http://localhost:8090/rest/server/containers/ess-kjar/processes/" + process_id + "/instances",
				HttpMethod.POST, httpEntity, Integer.class);
		int h = i.getBody();
		return h;
	}
	*/
	
	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${person.submitPerson}")
	private String PERSON_SUBMIT;

	public int submitPerson(String process_id, Person person, String username) {
		LinkedHashMap<String, Object> data = new LinkedHashMap<>();
		data.put("process_id", process_id);
		data.put("person", person);
		data.put("username", username);
		
		HttpEntity httpEntity = new HttpEntity<>(data);
		ResponseEntity<Integer> i = restTemplate.exchange(HOST_AND_PORT + PERSON_SUBMIT, HttpMethod.POST, httpEntity, Integer.class);
		int h = i.getBody();
		return h;
	}

}
