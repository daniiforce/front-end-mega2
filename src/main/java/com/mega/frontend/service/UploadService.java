package com.mega.frontend.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.Address;
import com.mega.frontend.utils.HeaderDataSetup;

@Service
public class UploadService {

	private final RestTemplate restTemplate;

	@Autowired
	public UploadService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${upload.save}")
	private String UPLOAD_SAVE;
	
	@Value("${upload.findByName}")
	private String UPLOAD_FINDBYNAME;
	
	public void uploadSave(String name, String base64) {
		
		LinkedHashMap<String, Object> data = new LinkedHashMap<>();
		data.put("name", name);
		data.put("base64", base64);
		
		HttpEntity httpEntity = new HttpEntity<>(data);

		restTemplate.exchange(HOST_AND_PORT + UPLOAD_SAVE, HttpMethod.POST, httpEntity, HttpStatus.class);
	}
	
	public String getBase64(String name) {		

		Map<String, String> mapBase64 = restTemplate.getForObject(HOST_AND_PORT + UPLOAD_FINDBYNAME + name, Map.class);

		String base64 = mapBase64.get("base64");
		return base64;
	}
	
}
