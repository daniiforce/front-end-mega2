package com.mega.frontend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mega.frontend.model.LastEducation;

@Service
public class LastEducationService {

	private final RestTemplate restTemplate;

	@Autowired
	public LastEducationService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Value("${host.and.port}")
	private String HOST_AND_PORT;

	@Value("${last_education.findall}")
	private String LAST_EDUCATION_FIND_ALL;

	public List<LastEducation> getAll() {
		ResponseEntity<?> lastEducation = restTemplate.getForEntity(HOST_AND_PORT + LAST_EDUCATION_FIND_ALL, List.class);
		List<LastEducation> lastEducations = (List<LastEducation>) lastEducation.getBody();
		return lastEducations;
	}

}
