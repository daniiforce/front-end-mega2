package com.mega.frontend.utils;

import java.nio.charset.Charset;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public class HeaderDataSetup {
	
	HttpHeaders httpHeaders;
	
	public HeaderDataSetup() {
	}
	
	public HeaderDataSetup(String username, String password) {
		this.httpHeaders = createHeaders(username, password);
	}
	
	
	HttpHeaders createHeaders(String username, String password){
		   return new HttpHeaders() {{
		         String auth = username + ":" + password;
		         byte[] encodedAuth = Base64.encodeBase64( 
		            auth.getBytes(Charset.forName("US-ASCII")) );
		         String authHeader = "Basic " + new String( encodedAuth );
		         set( "Authorization", authHeader );
		         setContentType(MediaType.APPLICATION_JSON);
		      }};
		}

	public HttpHeaders getHttpHeaders() {
		return httpHeaders;
	}

	public void setHttpHeaders(HttpHeaders httpHeaders) {
		this.httpHeaders = httpHeaders;
	}
	
}
