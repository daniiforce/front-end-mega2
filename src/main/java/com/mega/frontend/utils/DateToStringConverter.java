package com.mega.frontend.utils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DateToStringConverter {
	
	public static String dateToString(Date date) {
		LocalDate localStartDateTemp = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int yearStartDateTemp  = localStartDateTemp.getYear();
		int monthStartDateTemp = localStartDateTemp.getMonthValue();
		int dayStartDateTemp = localStartDateTemp.getDayOfMonth();

		String yearStartDateTempStr  = String.valueOf(yearStartDateTemp);
		String monthStartDateTempStr = "";
		String dayStartDateTempStr = "";
		
		if(monthStartDateTemp < 10) {
			monthStartDateTempStr = "0" + String.valueOf(monthStartDateTemp);
		}else {
			monthStartDateTempStr = String.valueOf(monthStartDateTemp);
		}

		if(dayStartDateTemp < 10) {
			dayStartDateTempStr = "0" + String.valueOf(dayStartDateTemp);
		}else {
			dayStartDateTempStr = String.valueOf(dayStartDateTemp);
		}
		
		return yearStartDateTempStr + monthStartDateTempStr + dayStartDateTempStr;
	}

}
