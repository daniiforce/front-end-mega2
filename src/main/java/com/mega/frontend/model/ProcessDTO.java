package com.mega.frontend.model;

public class ProcessDTO {

	private String id;
	private String name;
	private String version;
	private String packageProcess;
	private String container;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPackageProcess() {
		return packageProcess;
	}

	public void setPackageProcess(String packageProcess) {
		this.packageProcess = packageProcess;
	}

	public String getContainer() {
		return container;
	}

	public void setContainer(String container) {
		this.container = container;
	}

}
