package com.mega.frontend.model;

import java.util.Date;
import java.util.Map;

public class TasksDTO {

	// Tasks
	private String taskId;
	private String taskName;
	private String taskStatus;
	private String taskActualOwner;
	private String taskCreatedBy;
	private String taskProcInstId;
	private String taskProcDefId;
	private String taskContainerId;
	private String taskWorkItemId;
	private Date taskCreatedOn;

	// Workitems
	Map<String, Object> modelAtWorkItems;

	// Person
	private Person person;

	// Address
	private Address address;

	// Reward
	private Reward reward;

	// Work Experience
	private WorkingExperience workExperience;

	// Family
	private Family family;

	private String tempInfo;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public String getTaskActualOwner() {
		return taskActualOwner;
	}

	public void setTaskActualOwner(String taskActualOwner) {
		this.taskActualOwner = taskActualOwner;
	}

	public String getTaskCreatedBy() {
		return taskCreatedBy;
	}

	public void setTaskCreatedBy(String taskCreatedBy) {
		this.taskCreatedBy = taskCreatedBy;
	}

	public String getTaskProcInstId() {
		return taskProcInstId;
	}

	public void setTaskProcInstId(String taskProcInstId) {
		this.taskProcInstId = taskProcInstId;
	}

	public String getTaskProcDefId() {
		return taskProcDefId;
	}

	public void setTaskProcDefId(String taskProcDefId) {
		this.taskProcDefId = taskProcDefId;
	}

	public String getTaskContainerId() {
		return taskContainerId;
	}

	public void setTaskContainerId(String taskContainerId) {
		this.taskContainerId = taskContainerId;
	}

	public Map<String, Object> getModelAtWorkItems() {
		return modelAtWorkItems;
	}

	public void setModelAtWorkItems(Map<String, Object> modelAtWorkItems) {
		this.modelAtWorkItems = modelAtWorkItems;
	}

	public String getTaskWorkItemId() {
		return taskWorkItemId;
	}

	public void setTaskWorkItemId(String taskWorkItemId) {
		this.taskWorkItemId = taskWorkItemId;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getTempInfo() {
		return tempInfo;
	}

	public void setTempInfo(String tempInfo) {
		this.tempInfo = tempInfo;
	}

	public Date getTaskCreatedOn() {
		return taskCreatedOn;
	}

	public void setTaskCreatedOn(Date taskCreatedOn) {
		this.taskCreatedOn = taskCreatedOn;
	}

	public Reward getReward() {
		return reward;
	}

	public void setReward(Reward reward) {
		this.reward = reward;
	}

	public WorkingExperience getWorkExperience() {
		return workExperience;
	}

	public void setWorkExperience(WorkingExperience workExperience) {
		this.workExperience = workExperience;
	}

	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}

}
