package com.mega.frontend.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Address {

	private String landscape; // (nvarchar) 3

	private String empId; // (nvarchar) 8

	private String startDate; // (nvarchar) 8

	private String endDate; // (nvarchar) 8

	private String addressType; // (nvarchar) 2

	private String seq; // (nvarchar) 4

	private String street; // (nvarchar) 200

	private String location; // (nvarchar) 30

	private String province; // (nvarchar) 2

	private String country; // (nvarchar) 3

	private String postalcode; // (nvarchar) 5

	private String contact_person; // (nvarchar) 50

	private String telp_num; // (nvarchar) 25

	private String cellphone_num; // (nvarchar) 25

	private String user_change; // (nvarchar) 8

	private String last_change; // (nvarchar) 14

	private String created_by; // (nvarchar) 8

	private String created_date; // (nvarchar) 14

	private String kecamatan; // (nvarchar) 50

	private String kelurahan; // (nvarchar) 50

	private String rt; // (nvarchar) 50

	private String rw; // (nvarchar) 50

	private String city; // (nvarchar) 50

	private String kepemilikan; // (nvarchar) 50

	private String requestorId;

	private String requestorName;

	private String subject;

	private String description;

	private String employeeId;

	private String employeeName;

	private String comment;

	private String base64;

	private String countryStr;

	private String provinceStr;

	private String addressTypeStr;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date startDateTemp;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date endDateTemp;

	public String getLandscape() {
		return landscape;
	}

	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public String getContact_person() {
		return contact_person;
	}

	public void setContact_person(String contact_person) {
		this.contact_person = contact_person;
	}

	public String getTelp_num() {
		return telp_num;
	}

	public void setTelp_num(String telp_num) {
		this.telp_num = telp_num;
	}

	public String getCellphone_num() {
		return cellphone_num;
	}

	public void setCellphone_num(String cellphone_num) {
		this.cellphone_num = cellphone_num;
	}

	public String getUser_change() {
		return user_change;
	}

	public void setUser_change(String user_change) {
		this.user_change = user_change;
	}

	public String getLast_change() {
		return last_change;
	}

	public void setLast_change(String last_change) {
		this.last_change = last_change;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public String getKecamatan() {
		return kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	public String getRt() {
		return rt;
	}

	public void setRt(String rt) {
		this.rt = rt;
	}

	public String getRw() {
		return rw;
	}

	public void setRw(String rw) {
		this.rw = rw;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getKepemilikan() {
		return kepemilikan;
	}

	public void setKepemilikan(String kepemilikan) {
		this.kepemilikan = kepemilikan;
	}

	public String getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(String requestorId) {
		this.requestorId = requestorId;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public Date getStartDateTemp() {
		return startDateTemp;
	}

	public void setStartDateTemp(Date startDateTemp) {
		this.startDateTemp = startDateTemp;
	}

	public Date getEndDateTemp() {
		return endDateTemp;
	}

	public void setEndDateTemp(Date endDateTemp) {
		this.endDateTemp = endDateTemp;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getCountryStr() {
		return countryStr;
	}

	public void setCountryStr(String countryStr) {
		this.countryStr = countryStr;
	}

	public String getProvinceStr() {
		return provinceStr;
	}

	public void setProvinceStr(String provinceStr) {
		this.provinceStr = provinceStr;
	}

	public String getAddressTypeStr() {
		return addressTypeStr;
	}

	public void setAddressTypeStr(String addressTypeStr) {
		this.addressTypeStr = addressTypeStr;
	}

}
