package com.mega.frontend.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class Family {

	private String landscape;

	private String empId;

	private String start_date;

	private String end_date;

	private String seq;

	private String family_type;

	private String name;

	private String gender;

	private String birth_place;

	private String birth_date;

	private String date_of_death;

	private String last_education;

	private String address;

	private String telp_home;

	private String telp_office;

	private String occupation;

	private String medical_flag;

	private String no_surat;

	private String family_medical_code;

	private String no_asuransi;

	private String no_bpjs;

	private String no_cug;

	private String user_change;

	private String last_change;

	private String created_by;

	private String created_date;

	// Temporary

	private String requestorId;

	private String requestorName;

	private String subject;

	private String descriptionTemp;

	private String employeeId;

	private String employeeName;

	private String comment;

	private String base64;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date startDateTemp;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date endDateTemp;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthDateTemp;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateOfDeathTemp;

	public String getLandscape() {
		return landscape;
	}

	public void setLandscape(String landscape) {
		this.landscape = landscape;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}

	public String getUser_change() {
		return user_change;
	}

	public void setUser_change(String user_change) {
		this.user_change = user_change;
	}

	public String getLast_change() {
		return last_change;
	}

	public void setLast_change(String last_change) {
		this.last_change = last_change;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public String getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(String requestorId) {
		this.requestorId = requestorId;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public String getDescriptionTemp() {
		return descriptionTemp;
	}

	public void setDescriptionTemp(String descriptionTemp) {
		this.descriptionTemp = descriptionTemp;
	}

	public Date getStartDateTemp() {
		return startDateTemp;
	}

	public void setStartDateTemp(Date startDateTemp) {
		this.startDateTemp = startDateTemp;
	}

	public Date getEndDateTemp() {
		return endDateTemp;
	}

	public void setEndDateTemp(Date endDateTemp) {
		this.endDateTemp = endDateTemp;
	}

	public String getFamily_type() {
		return family_type;
	}

	public void setFamily_type(String family_type) {
		this.family_type = family_type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirth_place() {
		return birth_place;
	}

	public void setBirth_place(String birth_place) {
		this.birth_place = birth_place;
	}

	public String getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(String birth_date) {
		this.birth_date = birth_date;
	}

	public String getDate_of_death() {
		return date_of_death;
	}

	public void setDate_of_death(String date_of_death) {
		this.date_of_death = date_of_death;
	}

	public String getLast_education() {
		return last_education;
	}

	public void setLast_education(String last_education) {
		this.last_education = last_education;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelp_home() {
		return telp_home;
	}

	public void setTelp_home(String telp_home) {
		this.telp_home = telp_home;
	}

	public String getTelp_office() {
		return telp_office;
	}

	public void setTelp_office(String telp_office) {
		this.telp_office = telp_office;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getMedical_flag() {
		return medical_flag;
	}

	public void setMedical_flag(String medical_flag) {
		this.medical_flag = medical_flag;
	}

	public String getNo_surat() {
		return no_surat;
	}

	public void setNo_surat(String no_surat) {
		this.no_surat = no_surat;
	}

	public String getFamily_medical_code() {
		return family_medical_code;
	}

	public void setFamily_medical_code(String family_medical_code) {
		this.family_medical_code = family_medical_code;
	}

	public String getNo_asuransi() {
		return no_asuransi;
	}

	public void setNo_asuransi(String no_asuransi) {
		this.no_asuransi = no_asuransi;
	}

	public String getNo_bpjs() {
		return no_bpjs;
	}

	public void setNo_bpjs(String no_bpjs) {
		this.no_bpjs = no_bpjs;
	}

	public String getNo_cug() {
		return no_cug;
	}

	public void setNo_cug(String no_cug) {
		this.no_cug = no_cug;
	}

	public Date getBirthDateTemp() {
		return birthDateTemp;
	}

	public void setBirthDateTemp(Date birthDateTemp) {
		this.birthDateTemp = birthDateTemp;
	}

	public Date getDateOfDeathTemp() {
		return dateOfDeathTemp;
	}

	public void setDateOfDeathTemp(Date dateOfDeathTemp) {
		this.dateOfDeathTemp = dateOfDeathTemp;
	}

}
