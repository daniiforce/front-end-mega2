package com.mega.frontend.model;

import javax.validation.constraints.NotNull;

public class LoginDTO {

	@NotNull
	private String username;

	@NotNull
	private String password;

	private Boolean rememberMe;
	
	private String tempRememberMe;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getRememberMe() {
		return rememberMe;
	}

	public void setRememberMe(Boolean rememberMe) {
		this.rememberMe = rememberMe;
	}
	
	public String getTempRememberMe() {
		return tempRememberMe;
	}

	public void setTempRememberMe(String tempRememberMe) {
		this.tempRememberMe = tempRememberMe;
		if(!tempRememberMe.equals("") || tempRememberMe != null) {
			this.rememberMe = true;
		}else {
			this.rememberMe =false;
		}
	}

	@Override
	public String toString() {
		return "LoginDTO{" + "username='" + username + '\'' + ", password='" + password + '\'' + ", rememberMe="
				+ rememberMe + '}';
	}
}
