
$( document ).ready(function() {
	var dataLeasing = $("#txtLeasingCode").val();
	var isCheck = 0;
	$("#leasingCode").val(dataLeasing);
	var dataTables = $('#tblLeasingPic').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "bFilter": false,
        "ajax": {
            "url": "./get_list_leasing_pic",
            "data": function ( d ) {
            	d.leasingCode = dataLeasing,
            	d.isCheck = isCheck
			 //process data before sent to server.
         }},
      /* "columns": [
    	   			{ "data": "", "name" : "pilih" , "title" : ""},
                    { "data": "companyCode", "name" : "Company Code" , "title" : "Company Code"},
                    { "data": "companyAddress", "name" : "Company Address" , "title" : "Company Address"},
                    { "data": "leaderName", "name" : "Leader Name", "title" : "Leader Name"  },
                    { "data": "companyName", "name" : "Company Name" , "title" : "Company Name"},
                    { "data": "npwp", "name" : "NPWP" , "title" : "NPWP"},
                    { "data": "siup", "name" : "SIUP", "title" : "SIUP"  },
                    { "data": "isHolding", "name" : "Holding" , "title" : "isHolding"}
                ]    */
	});
	
	
	$('#btnDelLeasingPic').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_leasing_pic',
			type:'POST',
			data:{
				dipilih:pilihan
			},
			success:function(){
				dataTables.ajax.reload();
			}
		});
		//dataTables.ajax.reload();
	});
	
	
	$('#checkAllLeasingPic').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
	
	$('#btnSaveLeasingPic').click(function() {
		$.ajax({
			url    : './doAddLeasingPic',
			type   : 'POST',
			data   :{
				picName : $('#picName').val(),
				picTelp : $('#picTelp').val(),
				username : $('#username').val(),
				companyId : $("#cmpId option:selected").val(),
//				leasingId : $("#leasingId option:selected").val(),
				leasingCode : $('#leasingCode').val(),
				leasingDropPointId : $("#leasingDropPointId option:selected").val(),
				validFrom : $('#validFrom').val(),
				validTo : $('#validTo').val()
			},
			success: function(){
				dataTables.ajax.reload();
			}
		});
		dataTables.ajax.reload();
		$("#addLeasingPicEdit").hide();
		$("#addLeasingPic").hide();
		$("#searchLeasingPic").hide();
		$("#listLeasingPic").show();
	});
	
	$('#btnEditLeasingPic').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url 	: './edit_leasing_pic',
			data	:{
				dipilih:pilihan
			},
			success: function(data) {
				$("#idPicEdit").val(data.id);
				$("#picNameEdit").val(data.picName);
				$("#picTelpEdit").val(data.picTelp);
				$("#usernameEdit").val(data.username);
				 //$("#cmpIdEdit option[text=" + data.company.companyCode + " : " + data.company.companyName +"]").attr('selected', 'selected');
				$("#cmpIdEdit option[value='"+data.company.id+"']").prop('selected', true);
//				$("#leasingIdEdit option[value='"+data.leasing.id+"']").prop('selected', true);
				$('#leasingCodeEdit').val(data.leasing.leasingCode);
				$("#leasingDropPointIdEdit option[value='"+data.leasingDropPoint.id+"']").prop('selected', true);
				$('#validFromEdit').val(data.validFrom);
				$('#validToEdit').val(data.validTo);
			}
		});
		$("#addLeasingPicEdit").show();
		$("#addLeasingPic").hide();
		$("#searchLeasingPic").hide();
		$("#listLeasingPic").hide();
	});
	
	$("#btnSaveLeasingPicEdit").click(function(){
		$.ajax({
			url : './doEditLeasingPic',
			type   : 'POST',
			data : {
				id : $('#idPicEdit').val(),
				picName : $('#picNameEdit').val(),
				picTelp : $('#picTelpEdit').val(),
				username : $('#usernameEdit').val(),
//				leasingId : $("#leasingIdEdit option:selected").val(),
				leasingCode : $('#leasingCodeEdit').val(),
				leasingDropPointId : $("#leasingDropPointIdEdit option:selected").val(),
				companyId : $("#cmpIdEdit option:selected").val(),
				validFrom : $('#validFromEdit').val(),
				validTo : $('#validToEdit').val()
			},
			success:function(){
				dataTables.ajax.reload();
			}
		});
		dataTables.ajax.reload();
		$("#addLeasingPicEdit").hide();
		$("#addLeasingPic").hide();
		$("#searchLeasingPic").hide();
		$("#listLeasingPic").show();
		
	});
	
	$("#gotoLeasingPicList").click(function(){
		dataTables.ajax.reload();
		$("#addLeasingPicEdit").hide();
		$("#addLeasingPic").hide();
		$("#searchLeasingPic").hide();
		$("#listLeasingPic").show();
	});
	
	
	$("#btnAddLeasingPic").click(function(){
		$("#addLeasingPic").show();
		$("#searchLeasingPic").hide();
		$("#listLeasingPic").hide();
		$("#addLeasingPicEdit").hide();
	});
		
	$("#btnListLeasingPic").click(function(){
		dataTables.ajax.reload();
		$("#addLeasingPic").hide();
		$("#searchLeasingPic").hide();
		$("#listLeasingPic").show();
		$("#addLeasingPicEdit").hide();
	});
});