
$( document ).ready(function() {
	var approvalMasterId = $("#txtIdApprovalMaster").val();
	var isCheck = 0;
	$("#idApprovalMaster").val(approvalMasterId);
	var dataTables = $('#tblApprovalDetail').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "bFilter": false,
        "ajax": {
            "url": "./get_list_approval_detail",
            "data": function ( d ) {
            	d.id = approvalMasterId,
            	d.isCheck = isCheck
			 //process data before sent to server.
         }},
      /* "columns": [
    	   			{ "data": "", "name" : "pilih" , "title" : ""},
                    { "data": "companyCode", "name" : "Company Code" , "title" : "Company Code"},
                    { "data": "companyAddress", "name" : "Company Address" , "title" : "Company Address"},
                    { "data": "leaderName", "name" : "Leader Name", "title" : "Leader Name"  },
                    { "data": "companyName", "name" : "Company Name" , "title" : "Company Name"},
                    { "data": "npwp", "name" : "NPWP" , "title" : "NPWP"},
                    { "data": "siup", "name" : "SIUP", "title" : "SIUP"  },
                    { "data": "isHolding", "name" : "Holding" , "title" : "isHolding"}
                ]    */
	});
	
	
	$('#btnDelApprovalDetail').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_approval_detail',
			type:'POST',
			data:{
				dipilih:pilihan
			},
			success:function(){
				dataTables.ajax.reload();
			}
		});
		//dataTables.ajax.reload();
	});
	
	
	$('#checkAllApprovalDetail').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
	
	$('#btnSaveApprovalDetail').click(function() {
		$.ajax({
			url    : './doAddApprovalDetail',
			type   : 'POST',
			data   :{
				module : $('#module').val(),
				basedOn : $('#basedOn').val(),
				approvalNumber : $('#approvalNumber').val(),
				active : $('#active').val(),
				employeeId : $('#employeeId').val(),
				positionMasterId : $('#positionMasterId').val(),
				approvalMasterId : $('#idApprovalMaster').val()
			},
			success: function(){
				dataTables.ajax.reload();
			}
		});
		dataTables.ajax.reload();
		$("#addApprovalDetailEdit").hide();
		$("#addApprovalDetail").hide();
		$("#searchApprovalDetail").hide();
		$("#listApprovalDetail").show();
	});
	
	$('#btnEditApprovalDetail').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url 	: './edit_approval_detail',
			data	:{
				dipilih:pilihan
			},
			success: function(data) {
				$("#idApprovalDetailEdit").val(data.id);
				$("#moduleEdit").val(data.module);
				$("#idApprovalMasterEdit").val(data.approvalMasterId);
				$("#basedOnEdit").val(data.basedOn);
				$('#approvalNumberEdit').val(data.approvalNumber);
				$('#employeeIdEdit').val(data.employeeId);
				$('#positionMasterIdEdit').val(data.positionMasterId);
				$('#activeEdit').val(data.active);
			}
		});
		$("#addApprovalDetailEdit").show();
		$("#addApprovalDetail").hide();
		$("#searchApprovalDetail").hide();
		$("#listApprovalDetail").hide();
	});
	
	$("#btnSaveApprovalDetailEdit").click(function(){
		$.ajax({
			url : './doEditApprovalDetail',
			type   : 'POST',
			data : {
				id : $('#idApprovalDetailEdit').val(),
				module : $('#moduleEdit').val(),
				approvalMasterId : $('#idApprovalMasterEdit').val(),
				basedOn : $('#basedOnEdit').val(),
				approvalNumber : $('#approvalNumberEdit').val(),
				employeeId : $('#employeeIdEdit').val(),
				positionMasterId : $('#positionMasterIdEdit').val(),
				active : $('#activeEdit').val()
			},
			success:function(){
				dataTables.ajax.reload();
			}
		});
		dataTables.ajax.reload();
		$("#addApprovalDetailEdit").hide();
		$("#addApprovalDetail").hide();
		$("#searchApprovalDetail").hide();
		$("#listApprovalDetail").show();
		
	});
	
	$("#gotoApprovalDetailList").click(function(){
		dataTables.ajax.reload();
		$("#addApprovalDetailEdit").hide();
		$("#addApprovalDetail").hide();
		$("#searchApprovalDetail").hide();
		$("#listApprovalDetail").show();
	});
	
	
	$("#btnAddApprovalDetail").click(function(){
		$("#addApprovalDetailEdit").hide();
		$("#addApprovalDetail").show();
		$("#searchApprovalDetail").hide();
		$("#listApprovalDetail").hide();
	});
		
	$("#btnListApprovalDetail").click(function(){
		dataTables.ajax.reload();
		$("#addApprovalDetailEdit").hide();
		$("#addApprovalDetail").hide();
		$("#searchApprovalDetail").hide();
		$("#listApprovalDetail").show();
	});
});