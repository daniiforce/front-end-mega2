
$( document ).ready(function() {
	$("#listLeasingDropPoint").show();
	var dataLeasing = $("#txtLeasingCode").val();
	var isCheck = 0;
	$("#leasingCodeDropPoint").val(dataLeasing);
	var dataTables = $('#tblLeasingDropPoint').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "bFilter": false,
        "ajax": {
            "url": "./get_list_leasing_drop_point",
            "data": function ( d ) {
            	d.leasingCode = dataLeasing,
            	d.isCheck = isCheck
			 //process data before sent to server.
         }},
      /* "columns": [
    	   			{ "data": "", "name" : "pilih" , "title" : ""},
                    { "data": "companyCode", "name" : "Company Code" , "title" : "Company Code"},
                    { "data": "companyAddress", "name" : "Company Address" , "title" : "Company Address"},
                    { "data": "leaderName", "name" : "Leader Name", "title" : "Leader Name"  },
                    { "data": "companyName", "name" : "Company Name" , "title" : "Company Name"},
                    { "data": "npwp", "name" : "NPWP" , "title" : "NPWP"},
                    { "data": "siup", "name" : "SIUP", "title" : "SIUP"  },
                    { "data": "isHolding", "name" : "Holding" , "title" : "isHolding"}
                ]    */
	});
	
	
	$('#btnDelLeasingDropPoint').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_leasing_drop_point',
			type:'POST',
			data:{
				dipilih:pilihan
			},
			success:function(){
				dataTables.ajax.reload();
			}
		});
		//dataTables.ajax.reload();
	});
	
	
	$('#checkAllLeasingDropPoint').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
	
	$('#btnSaveLeasingDropPoint').click(function() {
		$.ajax({
			url    : './doAddLeasingDropPoint',
			type   : 'POST',
			data   :{
				dropPointCode : $('#dropPointCode').val(),
				dropPointName : $('#dropPointName').val(),
				dropPointLatitude : $('#dropPointLatitude').val(),
				dropPointLongitude : $('#dropPointLongitude').val(),
				companyId : $("#companyIdDropPoint option:selected").val(),
//				leasingId : $("#leasingIdDropPoint option:selected").val(),
				leasingCode : $('#leasingCodeDropPoint').val(),
				validFrom : $('#validFromDropPoint').val(),
				validTo : $('#validToDropPoint').val()
			},
			success: function(){
				dataTables.ajax.reload();
			}
		});
		dataTables.ajax.reload();
		$("#addLeasingDropPointEdit").hide();
		$("#addLeasingDropPoint").hide();
		$("#searchLeasingDropPoint").hide();
		$("#listLeasingDropPoint").show();
	});
	
	$('#btnEditLeasingDropPoint').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url 	: './edit_leasing_drop_point',
			data	:{
				dipilih:pilihan
			},
			success: function(data) {
				$("#idDropPointEdit").val(data.id);
				$("#dropPointCodeEdit").val(data.dropPointCode);
				$("#dropPointNameEdit").val(data.dropPointName);
				$("#dropPointLatitudeEdit").val(data.dropPointLatitude);
				$("#dropPointLongitudeEdit").val(data.dropPointLongitude);
				 //$("#cmpIdEdit option[text=" + data.company.companyCode + " : " + data.company.companyName +"]").attr('selected', 'selected');
				$("#companyIdDropPointEdit option[value='"+data.company.id+"']").prop('selected', true);
//				$("#leasingIdDropPointEdit option[value='"+data.leasing.id+"']").prop('selected', true);
				$('#leasingCodeDropPointEdit').val(data.leasing.leasingCode);
				$('#validFromDropPointEdit').val(data.strValidFrom);
				$('#validToDropPointEdit').val(data.strValidTo);
			}
		});
		$("#addLeasingDropPointEdit").show();
		$("#addLeasingDropPoint").hide();
		$("#searchLeasingDropPoint").hide();
		$("#listLeasingDropPoint").hide();
	});
	
	$("#btnSaveLeasingDropPointEdit").click(function(){
		$.ajax({
			url : './doEditLeasingDropPoint',
			type   : 'POST',
			data : {
				id : $('#idDropPointEdit').val(),
				dropPointCode : $('#dropPointCodeEdit').val(),
				dropPointName : $('#dropPointNameEdit').val(),
				dropPointLatitude : $('#dropPointLatitudeEdit').val(),
				dropPointLongitude : $('#dropPointLongitudeEdit').val(),
//				leasingId : $("#leasingIdDropPointEdit option:selected").val(),
				leasingCode : $('#leasingCodeDropPointEdit').val(),
				companyId : $("#companyIdDropPointEdit option:selected").val(),
				validFrom : $('#validFromDropPointEdit').val(),
				validTo : $('#validToDropPointEdit').val()
			},
			success:function(){
				dataTables.ajax.reload();
			}
		});
		dataTables.ajax.reload();
		$("#addLeasingDropPointEdit").hide();
		$("#addLeasingDropPoint").hide();
		$("#searchLeasingDropPoint").hide();
		$("#listLeasingDropPoint").show();
		
	});
	
	$("#gotoLeasingDropPointList").click(function(){
		dataTables.ajax.reload();
		$("#addLeasingDropPointEdit").hide();
		$("#addLeasingDropPoint").hide();
		$("#searchLeasingDropPoint").hide();
		$("#listLeasingDropPoint").show();
	});
	
	
	$("#btnAddLeasingDropPoint").click(function(){
		$("#addLeasingDropPoint").show();
		$("#searchLeasingDropPoint").hide();
		$("#listLeasingDropPoint").hide();
		$("#addLeasingDropPointEdit").hide();
	});
		
	$("#btnListLeasingDropPoint").click(function(){
		dataTables.ajax.reload();
		$("#addLeasingDropPoint").hide();
		$("#searchLeasingDropPoint").hide();
		$("#listLeasingDropPoint").show();
		$("#addLeasingDropPointEdit").hide();
	});
});