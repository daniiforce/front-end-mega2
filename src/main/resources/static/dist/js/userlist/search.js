$(document).ready(function () {
	var table = $("#tableSearchUser");
	var isCheck = 0;
	var tableSearch = table.DataTable({
		"processing": true,
        "serverSide": true,
        "bFilter": false,
        "ajax": {
        	"type":"GET",
            "url": "./get_list_user",
            "data": function(d){
            	d.username= $("#username").val(),
            	d.firstName= $("#firstName").val(),
            	d.lastName= $("#lastName").val(),
            	d.isCheck=isCheck
            		},
        },
	});
	
	$("#btnCari").click(function() {
		tableSearch.ajax.reload();
	});
	
	$('#checkAllUser').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
});
