
$( document ).ready(function() {
	var isCheck = 0;
	var dataTables = $('#tblCompany').DataTable( {
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "pageLength": 5,
        "ajax": {
        	"type": "POST",
            "url": "./get_list_company",
            "data": function ( data ) {
            	data.isCheck = isCheck
			 //process data before sent to server.
         }},
         /*"columns": [
             { "data": "companyCode", "name" : "Company Code", "title" : "Company Code"  },
             { "data": "companyName", "name" : "Company Name" , "title" : "Company Name"},
             { "data": "companyAddress", "name" : "Company Address" , "title" : "Company Address"},
             { "data": "leaderName", "name" : "Leader Name" , "title" : "Leader Name"},
             { "data": "siup", "name" : "SIUP" , "title" : "SIUP"},
             { "data": "npwp", "name" : "NPWP" , "title" : "NPWP"},
             { "data": "isHolding", "name" : "Holding" , "title" : "Holding"}
             
         ]    */
	});
	
	
	$('#deleteId').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_company',
			type:'POST',
			data:{
				dipilih:pilihan
			},
			error :function(data) {
				if(data.status === 422 ) {
					alert("The delete action cannot be done because the company is being used in another module");
				}
			}
//			,
//			success:function(data){
//				if(data.status === 200 ) {
//					alert("Delete successfull");
//				}
//			}
		});
//		dataTables.ajax.reload();
	});
	
	
	$('#checkAll').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
	
	
});