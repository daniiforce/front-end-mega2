
$( document ).ready(function() {
	var dataEmp = $("#txtEmpCode").val();
	var isCheck = 0;
	$("#empCodePeriod").val(dataEmp);
	var dataTables = $('#tblEmpPeriod').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "bFilter": false,
        "ajax": {
            "url": "./get_list_emp_period",
            "data": function ( d ) {
            	d.empCode = dataEmp,
            	d.isCheck = isCheck
			 //process data before sent to server.
         }},
      /* "columns": [
    	   			{ "data": "", "name" : "pilih" , "title" : ""},
                    { "data": "companyCode", "name" : "Company Code" , "title" : "Company Code"},
                    { "data": "companyAddress", "name" : "Company Address" , "title" : "Company Address"},
                    { "data": "leaderName", "name" : "Leader Name", "title" : "Leader Name"  },
                    { "data": "companyName", "name" : "Company Name" , "title" : "Company Name"},
                    { "data": "npwp", "name" : "NPWP" , "title" : "NPWP"},
                    { "data": "siup", "name" : "SIUP", "title" : "SIUP"  },
                    { "data": "isHolding", "name" : "Holding" , "title" : "isHolding"}
                ]    */
	});
	
	
	$('#btnDelEmpPeriod').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_emp_period',
			type:'POST',
			data:{
				dipilih:pilihan
			},
			success:function(){
				dataTables.ajax.reload();
			}
		});
		//dataTables.ajax.reload();
	});
	
	
	$('#checkAllEmpPeriod').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
	
	$('#btnSaveEmpPeriod').click(function() {
		$.ajax({
			url    : './doAddEmpPeriod',
			type   : 'POST',
			data   :{
				empCode : $('#empCodePeriod').val(),
				companyId : $("#cmpId option:selected").val(),
				validFrom : $('#validFrom').val(),
				validTo : $('#validTo').val()
			},
			success: function(){
				dataTables.ajax.reload();
			}
		});
		dataTables.ajax.reload();
		$("#addEmpPeriodEdit").hide();
		$("#addEmpPeriod").hide();
		$("#searchEmpPeriod").hide();
		$("#listEmpPeriod").show();
	});
	
	$('#btnEditEmpPeriod').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url 	: './edit_emp_period',
			data	:{
				dipilih:pilihan
			},
			success: function(data) {
				//$("#idEmpPeriodHidden").val(data.id);
				$("#empCodePeriodEdit").val(data.empCode);
				 //$("#cmpIdEdit option[text=" + data.company.companyCode + " : " + data.company.companyName +"]").attr('selected', 'selected');
				$("#cmpIdEdit option[value='"+data.company.id+"']").prop('selected', true);
				$('#validFromEdit').val(data.strValidFrom);
				$('#validToEdit').val(data.strValidTo);
			}
		});
		$("#addEmpPeriodEdit").show();
		$("#addEmpPeriod").hide();
		$("#searchEmpPeriod").hide();
		$("#listEmpPeriod").hide();
	});
	
	$("#btnSaveEmpPeriodEdit").click(function(){
		$.ajax({
			url : './editAddEmpPeriod',
			type   : 'POST',
			data : {
				//id : $("#idEmpPeriodHidden").val(),
				empCode : $("#empCodePeriodEdit").val(),
				companyId : $("#cmpIdEdit option:selected").val(),
				validFrom : $('#validFromEdit').val(),
				validTo : $('#validToEdit').val()
			},
			success:function(){
				dataTables.ajax.reload();
			}
		});
		dataTables.ajax.reload();
		$("#addEmpPeriodEdit").hide();
		$("#addEmpPeriod").hide();
		$("#searchEmpPeriod").hide();
		$("#listEmpPeriod").show();
		
	});
	
	$("#gotoList").click(function(){
		dataTables.ajax.reload();
		$("#addEmpPeriodEdit").hide();
		$("#addEmpPeriod").hide();
		$("#searchEmpPeriod").hide();
		$("#listEmpPeriod").show();
	});
	
	
	$("#btnAddEmpPeriod").click(function(){
		$("#addEmpPeriod").show();
		$("#searchEmpPeriod").hide();
		$("#listEmpPeriod").hide();
		$("#addEmpPeriodEdit").hide();
	});
		
	$("#btnListEmpPeriod").click(function(){
		dataTables.ajax.reload();
		$("#addEmpPeriod").hide();
		$("#searchEmpPeriod").hide();
		$("#listEmpPeriod").show();
		$("#addEmpPeriodEdit").hide();
	});
});