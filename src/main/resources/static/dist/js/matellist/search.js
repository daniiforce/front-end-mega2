$(document).ready(function () {
	var table = $("#tblMatel");
	var tableSearch = table.DataTable({
		"processing": true,
        "serverSide": true,
        "bFilter": false,
        "ajax": {
        	"type":"GET",
            "url": "./get_list_matel",
            "data": function(d){
            	d.matelCode= $("#matelCode").val(),
            	d.matelName= $("#matelName").val()
            },
        },
	});
	
	$("#searchMatel").click(function() {
		tableSearch.ajax.reload();
	});
	
});
