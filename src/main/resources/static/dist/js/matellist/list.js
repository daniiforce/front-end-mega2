
$( document ).ready(function() {
	var dataTables = $('#tblMatel').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "bFilter": false,
        "ajax": {
            "url": "./get_list_matel",
            "data": function ( d ) {
            	d.matelCode= $("#matelCode").val(),
            	d.matelName= $("#matelName").val()
         }},
	});
	
	$("#btnCari").click(function() {
		dataTables.ajax.reload();
	});
	
	$("#dropdown_company").change(function(){
	      $.ajax(
	    	{
	    		type: "POST",
	    		contentType: "application/json; charset=utf-8",
	    		url:'./populate_branch?companyCode=' + document.getElementById("dropdown_company").value,
	    		data: {},
	    		dataType: "json",
				
				success: function(response){
					var len = response.length;

	                $("#dropdown_branch").empty();
	                var selectBranch = 'Select Branch';
	                $("#dropdown_branch").append("<option value=''>"+selectBranch+"</option>");
	                for( var i = 0; i<len; i++){
	                    var code = response[i]['branchCode'];
	                    var name = response[i]['branchName'];
	                    var mixName = code + ':' + name;
	                    
	                    $("#dropdown_branch").append("<option value='"+code+"'>"+mixName+"</option>");
	                }
			      }
			}
	      );
	});
	
});