
$( document ).ready(function() {
	var dataTables = $('#tblEmpType').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "ajax": {
            "url": "./get_list_emp_type",
            "data": function ( data ) {
			 //process data before sent to server.
         }},
      /* "columns": [
    	   			{ "data": "", "name" : "pilih" , "title" : ""},
                    { "data": "companyCode", "name" : "Company Code" , "title" : "Company Code"},
                    { "data": "companyAddress", "name" : "Company Address" , "title" : "Company Address"},
                    { "data": "leaderName", "name" : "Leader Name", "title" : "Leader Name"  },
                    { "data": "companyName", "name" : "Company Name" , "title" : "Company Name"},
                    { "data": "npwp", "name" : "NPWP" , "title" : "NPWP"},
                    { "data": "siup", "name" : "SIUP", "title" : "SIUP"  },
                    { "data": "isHolding", "name" : "Holding" , "title" : "isHolding"}
                ]    */
	});
	
	
	$('#deleteIdEmpType').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_emp_type',
			type:'POST',
			data:{
				dipilih:pilihan
			}
		});
		dataTables.ajax.reload();
	});
	
	
	$('#checkAllEmpType').click(function(){
		if($(this).is(":checked"))
			$('.chkCheckBoxId').prop('checked', true);
		else
			$('.chkCheckBoxId').prop('checked', false);
	});
	
	
});