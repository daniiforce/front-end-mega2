$(document).ready(function () {
	var table = $("#tableSearchPosMaster");
	var isCheck = 0;
	var tableSearch = table.DataTable({
		"processing": true,
        "serverSide": true,
        "bFilter": false,
        "ajax": {
        	"type":"GET",
            "url": "./get_list_pos_master",
            "data": function(d){
            	d.positionCode= $("#positionCode").val(),
            	d.positionName= $("#positionName").val(),
            	d.isCheck=isCheck
            		},
        },
	});
	
	$("#btnCari").click(function() {
		tableSearch.ajax.reload();
	});
	
	$('#checkAllPosMaster').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
});
