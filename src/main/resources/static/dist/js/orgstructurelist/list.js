
$( document ).ready(function() {
	var isCheck = 0;
	var dataTables = $('#tblOrgStructure').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "bFilter": false,
        "ajax": {
            "url": "./get_list_org_structure",
            "data": function ( data ) {
            	data.isCheck = isCheck
			 //process data before sent to server.
         }},
	});
	
	
	$('#deleteIdOrgStruct').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_org_structure',
			type:'POST',
			data:{
				dipilih:pilihan
			}
		});
		dataTables.ajax.reload();
	});
	
	
	$('#checkAllOrgStruct').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
	
	
});

