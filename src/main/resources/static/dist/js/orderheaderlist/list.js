
$( document ).ready(function() {
	$("#tblOrderDetail").show();
	var isCheck = 0;
	var dataTables = $('#tblOrderHeader').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "bFilter": false,
        "ajax": {
            "url": "./get_list_order_header",
            "data": function ( d ) {
            	d.orderHeaderNo= $("#orderHeaderNo").val(),
            	d.bastkNo= $("#bastkNo").val(),
            	d.isCheck=isCheck
         }},
	});
	
	
	$('#deleteOrderHeaderId').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_order_header',
			type:'POST',
			data:{
				dipilih:pilihan
			}
		});
		dataTables.ajax.reload();
	});
	
	
	$('#"checkAllOrderHeader"').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
	
});