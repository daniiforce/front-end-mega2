$(document).ready(function () {
	var table = $("#tblOrderHeaderSearch");
	var isCheck = 0;
	var tableSearch = table.DataTable({
		"processing": true,
        "serverSide": true,
        "bFilter": false,
        "ajax": {
        	"type":"GET",
            "url": "./get_list_order_header",
            "data": function(d){
            	d.orderHeaderNo= $("#orderHeaderNo").val(),
            	d.bastkNo= $("#bastkNo").val(),
            	d.isCheck=isCheck
            		},
        },
	});
	
	$("#searchOrderHeader").click(function() {
		tableSearch.ajax.reload();
	});
	
	$('#checkAllOrderHeader').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
});
