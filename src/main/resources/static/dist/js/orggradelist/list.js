
$( document ).ready(function() {
	var isCheck = 0;
	var dataTables = $('#tblOrgGrade').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "bFilter": false,
        "ajax": {
            "url": "./get_list_org_grade",
            "data": function ( data ) {
            	data.isCheck = isCheck
			 //process data before sent to server.
         }},
	});
	
	
	$('#deleteIdOrgGrade').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_all_orggrade',
			type:'POST',
			data:{
				dipilih:pilihan
			}
		});
		dataTables.ajax.reload();
	});
	
	
	$('#checkAllOrgGrade').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
	
	
});

