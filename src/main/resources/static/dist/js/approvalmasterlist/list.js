
$( document ).ready(function() {
	$("#tblApprovalDetail").show();
	var isCheck = 0;
	var dataTables = $('#tblApprovalMaster').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "bFilter": false,
        "ajax": {
            "url": "./get_list_approval_master",
            "data": function ( d ) {
            	d.level = $("#level").val(),
            	d.isCheck=isCheck
         }},
	});
	
	
	$('#deleteApprovalMasterId').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_all_approval_master',
			type:'POST',
			data:{
				dipilih:pilihan
			},
			success:function(){
				dataTables.ajax.reload();
			}
		});
	});
	
	
	$('#"checkAllApprovalMaster"').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
	
});