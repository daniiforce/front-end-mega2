$(document).ready(function () {
	var table = $("#tblApprovalMasterSearch");
	var isCheck = 0;
	var tableSearch = table.DataTable({
		"processing": true,
        "serverSide": true,
        "bFilter": false,
        "ajax": {
        	"type":"GET",
            "url": "./get_list_approval_master",
            "data": function(d){
            	d.level = $("#level").val(),
            	d.isCheck=isCheck
            		},
        },
	});
	
	$("#searchApprovalMaster").click(function() {
		tableSearch.ajax.reload();
	});
	
	$('#checkAllApprovalMaster').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
});
