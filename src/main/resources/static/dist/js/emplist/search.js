$(document).ready(function () {
	var table = $("#tblEmp");
	var isCheck = 0;
	var tableSearch = table.DataTable({
		"processing": true,
        "serverSide": true,
        "bFilter": false,
        "ajax": {
        	"type":"GET",
            "url": "./get_list_emp",
            "data": function(d){
            	d.orgGradeCode= $("#orgGradeCode").val(),
            	d.orgGradeName= $("#orgGradeName").val(),
            	d.isCheck=isCheck
            		},
        },
	});
	
	$("#searchEmp").click(function() {
		tableSearch.ajax.reload();
	});
	
	$('#checkAllOrgGrade').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
});
