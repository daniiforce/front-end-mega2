
$( document ).ready(function() {
	$("#listEmpPeriod").show();
	var isCheck = 0;
	var dataTables = $('#tblEmp').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "bFilter": false,
        "ajax": {
            "url": "./get_list_emp",
            "data": function ( d ) {
            	d.empCode= $("#empCode").val(),
            	d.empName= $("#empName").val(),
            	d.company= $("#company").val(),
            	d.isCheck=isCheck
         }},
     
	});
	
	
	$('#deleteEmpId').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_emp',
			type:'POST',
			data:{
				dipilih:pilihan
			}
		});
		dataTables.ajax.reload();
	});
	
	
	$('#checkAllEmp').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
	
	$("#btnCari").click(function() {
		dataTables.ajax.reload();
	});
	
	
});