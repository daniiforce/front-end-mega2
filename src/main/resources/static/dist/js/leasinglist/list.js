
$( document ).ready(function() {
	$("#listLeasingPic").show();
	var isCheck = 0;
	var dataTables = $('#tblLeasingMaster').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "bFilter": false,
        "ajax": {
            "url": "./get_list_leasing",
            "data": function ( d ) {
            	d.leasingCode= $("#leasingCode").val(),
            	d.leasingName= $("#leasingName").val(),
            	d.isCheck=isCheck
         }},
	});
	
	
	$('#deleteLeasingId').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_leasing',
			type:'POST',
			data:{
				dipilih:pilihan
			}
		});
		dataTables.ajax.reload();
	});
	
	
	$('#checkAllLeasing').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
	
//	$("#btnCari").click(function() {
//		dataTables.ajax.reload();
//	});
	
	
});