$(document).ready(function () {
	var table = $("#tblLeasingMasterSearch");
	var isCheck = 0;
	var tableSearch = table.DataTable({
		"processing": true,
        "serverSide": true,
        "bFilter": false,
        "ajax": {
        	"type":"GET",
            "url": "./get_list_leasing",
            "data": function(d){
            	d.leasingCode= $("#leasingCode").val(),
            	d.leasingName= $("#leasingName").val(),
            	d.isCheck=isCheck
            		},
        },
	});
	
	$("#searchLeasing").click(function() {
		tableSearch.ajax.reload();
	});
	
	$('#checkAllLeasing').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
});
