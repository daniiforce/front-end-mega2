$(document).ready(function () {
	var table = $("#tableSearchPosStructure");
	var isCheck = 0;
	var tableSearch = table.DataTable({
		"processing": true,
        "serverSide": true,
        "bFilter": false,
        "ajax": {
        	"type":"GET",
            "url": "./get_list_pos_structure",
            "data": function(d){
            	d.posCode= $("#posCode").val(),
            	d.parentPosition= $("#parentPosition").val(),
            	d.company= $("#company").val(),
            	d.isCheck=isCheck
            		},
        },
	});
	
	$("#btnCari").click(function() {
		tableSearch.ajax.reload();
	});
	
	$('#checkAllPosStructure').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}
		else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
});
