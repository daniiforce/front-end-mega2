
$( document ).ready(function() {
	var isCheck = 0;
	var dataTables = $('#tblPosGrade').DataTable( {
        "processing": true,
        "serverSide": true,
        "bFilter": false,
        "pageLength": 5,
        "ajax": {
            "url": "./get_list_pos_grade",
            "data": function ( data ) {
            	data.isCheck = isCheck
			 //process data before sent to server.
         }},
      /* "columns": [
    	   			{ "data": "", "name" : "pilih" , "title" : ""},
                    { "data": "companyCode", "name" : "Company Code" , "title" : "Company Code"},
                    { "data": "companyAddress", "name" : "Company Address" , "title" : "Company Address"},
                    { "data": "leaderName", "name" : "Leader Name", "title" : "Leader Name"  },
                    { "data": "companyName", "name" : "Company Name" , "title" : "Company Name"},
                    { "data": "npwp", "name" : "NPWP" , "title" : "NPWP"},
                    { "data": "siup", "name" : "SIUP", "title" : "SIUP"  },
                    { "data": "isHolding", "name" : "Holding" , "title" : "isHolding"}
                ]    */
	});
	
	
	$('#deleteIdPosGrade').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_all_pos_grade',
			type:'POST',
			data:{
				dipilih:pilihan
			}
		});
//		dataTables.ajax.reload();
	});
	
	
	$('#checkAllPosGrade').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}
		else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
	
	
});

