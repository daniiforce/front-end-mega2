$(document).ready(function () {
	var table = $("#tableSearchUserRole");
	var isCheck = 0;
	var tableSearch = table.DataTable({
		"processing": true,
        "serverSide": true,
        "bFilter": false,
        "ajax": {
        	"type":"GET",
            "url": "./get_list_user_role",
            "data": function(d){
            	d.username= $("#username").val(),
            	d.label= $("#label").val(),
            	d.isCheck=isCheck
            		},
        },
	});
	
	$("#btnCariUserRole").click(function() {
		tableSearch.ajax.reload();
	});
	
	$('#checkAllUserRole').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
});
