
$( document ).ready(function() {
	var orderHeader = $("#txtOrderHeaderNo").val();
	var isCheck = 0;
	$("#orderHeaderNo").val(orderHeader);
	var dataTables = $('#tblOrderDetail').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "bFilter": false,
        "ajax": {
            "url": "./get_list_order_detail",
            "data": function ( d ) {
            	d.orderHeaderNo = orderHeader,
            	d.isCheck = isCheck
			 //process data before sent to server.
         }},
      /* "columns": [
    	   			{ "data": "", "name" : "pilih" , "title" : ""},
                    { "data": "companyCode", "name" : "Company Code" , "title" : "Company Code"},
                    { "data": "companyAddress", "name" : "Company Address" , "title" : "Company Address"},
                    { "data": "leaderName", "name" : "Leader Name", "title" : "Leader Name"  },
                    { "data": "companyName", "name" : "Company Name" , "title" : "Company Name"},
                    { "data": "npwp", "name" : "NPWP" , "title" : "NPWP"},
                    { "data": "siup", "name" : "SIUP", "title" : "SIUP"  },
                    { "data": "isHolding", "name" : "Holding" , "title" : "isHolding"}
                ]    */
	});
	
	
	$('#btnDelOrderDetail').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_order_detail',
			type:'POST',
			data:{
				dipilih:pilihan
			},
			success:function(){
				dataTables.ajax.reload();
			}
		});
		//dataTables.ajax.reload();
	});
	
	
	$('#checkAllOrderDetail').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
	
	$('#btnSaveOrderDetail').click(function() {
		$.ajax({
			url    : './doAddOrderDetail',
			type   : 'POST',
			data   :{
				orderHeaderNo : $('#orderHeaderNo').val(),
				step : $('#step').val(),
				state : $('#state').val(),
				keterangan : $('#keterangan').val(),
				stepNo : $('#stepNo').val(),
				start : $('#start').val(),
				finish : $('#finish').val()
			},
			success: function(){
				dataTables.ajax.reload();
			}
		});
		dataTables.ajax.reload();
		$("#addOrderDetailEdit").hide();
		$("#addOrderDetail").hide();
		$("#searchOrderDetail").hide();
		$("#listOrderDetail").show();
	});
	
	$('#btnEditOrderDetail').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url 	: './edit_order_detail',
			data	:{
				dipilih:pilihan
			},
			success: function(data) {
				$("#idOrderDetailEdit").val(data.id);
				$("#orderHeaderNoEdit").val(data.orderHeaderNo);
				$("#stepEdit").val(data.step);
				$("#stepNoEdit").val(data.stepNo);
				$('#keteranganEdit').val(data.keterangan);
				$('#stateEdit').val(data.state);
				$('#startEdit').val(data.start);
				$('#finishEdit').val(data.finish);
			}
		});
		$("#addOrderDetailEdit").show();
		$("#addOrderDetail").hide();
		$("#searchOrderDetail").hide();
		$("#listOrderDetail").hide();
	});
	
	$("#btnSaveOrderDetailEdit").click(function(){
		$.ajax({
			url : './doEditOrderHeader',
			type   : 'POST',
			data : {
				id : $('#idOrderDetailEdit').val(),
				orderHeaderNo : $('#orderHeaderNoEdit').val(),
				step : $('#stepEdit').val(),
				stepNo : $('#stepNoEdit').val(),
				keterangan : $('#keteranganEdit').val(),
				state : $('#stateEdit').val(),
				start : $('#startEdit').val(),
				finish : $('#finishEdit').val()
			},
			success:function(){
				dataTables.ajax.reload();
			}
		});
		dataTables.ajax.reload();
		$("#addOrderDetailEdit").hide();
		$("#addOrderDetail").hide();
		$("#searchOrderDetail").hide();
		$("#listOrderDetail").show();
		
	});
	
	$("#gotoOrderDetailList").click(function(){
		dataTables.ajax.reload();
		$("#addOrderDetailEdit").hide();
		$("#addOrderDetail").hide();
		$("#searchOrderDetail").hide();
		$("#listOrderDetail").show();
	});
	
	
	$("#btnAddOrderDetail").click(function(){
		$("#addOrderDetailEdit").hide();
		$("#addOrderDetail").show();
		$("#searchOrderDetail").hide();
		$("#listOrderDetail").hide();
	});
		
	$("#btnListOrderDetail").click(function(){
		dataTables.ajax.reload();
		$("#addOrderDetailEdit").hide();
		$("#addOrderDetail").hide();
		$("#searchOrderDetail").hide();
		$("#listOrderDetail").show();
	});
});