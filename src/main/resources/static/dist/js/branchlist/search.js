$(document).ready(function () {
	var table = $("#tableSearchBranch");
	var isCheck = 0;
	var tableSearch = table.DataTable({
		"processing": true,
        "serverSide": true,
        "bFilter": false,
        "ajax": {
        	"type":"GET",
            "url": "./get_list_branch",
            "data": function(d){
            	d.branchCode= $("#branchCode").val(),
            	d.branchName= $("#branchName").val(),
            	d.isCheck=isCheck
            		},
        },
	});
	
	$("#btnCari").click(function() {
		tableSearch.ajax.reload();
	});
	
	$('#checkAllBranch').click(function(){
		if($(this).is(":checked")){
			$('.chkCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
});
