
$( document ).ready(function() {
	$("#listEmpOrg").show();
	var dataEmp = $("#txtEmpCode").val();
	$("#empCodeOrg").val(dataEmp);
	isCheck = 0;
	var dataTablesEmpOrg = $('#tblEmpOrg').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "bFilter": false,
        "ajax": {
            "url": "./get_list_emp_org",
            "data": function ( d ) {
            	d.empCode= $("#empCodeOrg").val(),
            	d.company= $("#company").val(),
            	d.isCheck=isCheck
			 //process data before sent to server.
         }},
      /* "columns": [
    	   			{ "data": "", "name" : "pilih" , "title" : ""},
                    { "data": "companyCode", "name" : "Company Code" , "title" : "Company Code"},
                    { "data": "companyAddress", "name" : "Company Address" , "title" : "Company Address"},
                    { "data": "leaderName", "name" : "Leader Name", "title" : "Leader Name"  },
                    { "data": "companyName", "name" : "Company Name" , "title" : "Company Name"},
                    { "data": "npwp", "name" : "NPWP" , "title" : "NPWP"},
                    { "data": "siup", "name" : "SIUP", "title" : "SIUP"  },
                    { "data": "isHolding", "name" : "Holding" , "title" : "isHolding"}
                ]    */
	});
	
	
	$('#deleteIdEmpOrg').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_emp',
			type:'POST',
			data:{
				dipilih:pilihan
			}
		});
		dataTablesEmpOrg.ajax.reload();
	});
	
	
	$('#checkAllEmpOrg').click(function(){
		if($(this).is(":checked")){
			$('.chkEmpOrgCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkEmpOrgCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
	
	$("#btnDoSaveEmpOrg").click(function(){
		$.ajax({
			url  : './doAddEmpOrganization',
			type : 'POST',
			data : {
				empCode : $("#txtEmpCode").val(),
				cmpId : $("#companyID option:selected").val(),
				orgId : $("#orgMasterId option:selected").val(),
				validFrom : $('#validFromAddEmpOrg').val(),
				validTo : $('#validToAddEmpOrg').val()
			},
			success: function(){
				dataTablesEmpOrg.ajax.reload();
			}
		});
		$("#addEmpOrg").hide();
		$("#listEmpOrg").show();
		$("#editEmpOrg").hide();
	});
	
	$("#btnDelEmpOrg").click(function(){
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url : './delete_emp_org',
			type : 'POST',
			data:{
				dipilih:pilihan
			},
			success: function(){
				dataTablesEmpOrg.ajax.reload();	
			}
		});
	});
	
	$("#btnEditEmpOrg").click(function(){
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url		: './showeditform',
			data	:{
				dipilih:pilihan
			},
			success : function(data){
				$("#idEmpOrg").val(data.id);
				$("#empCodeOrgEdit").val(data.employeeCode);
				$("#orgMasterIdEditEmpOrg option[value='"+data.orgMasterId+"']").prop('selected', true);
				$("#companyIDEditEmpOrg option[value='"+data.companyId+"']").prop('selected', true);
				$("#validFromEmpOrgEdit").val(data.strValidFrom);
				$("#validToEmpOrgEdit").val(data.strValidTo);
			}
		});
		$("#addEmpOrg").hide();
		$("#listEmpOrg").hide();
		$("#editEmpOrg").show();
	});
	
	$("#btnSaveEmpOrgEdit").click(function(){
		$.ajax({
			url		: './doEditEmpOrg',
			type 	: 'POST',
			data	:{
				id : $("#idEmpOrg").val(),
				empCode : $("#empCodeOrgEdit").val(),
				companyId : $("#companyIDEditEmpOrg option:selected").val(),
				orgid : $("#orgMasterIdEditEmpOrg option:selected").val(),
				validFrom : $('#validFromEmpOrgEdit').val(),
				validTo : $('#validToEmpOrgEdit').val()
			},success:function(){
				isCheck = 0;
				dataTablesEmpOrg.ajax.reload();
			}
		});
		$("#addEmpOrg").hide();
		$("#listEmpOrg").show();
		$("#editEmpOrg").hide();
	});
	
	$("#btnAddEmpOrg").click(function(){
		dataTablesEmpOrg.ajax.reload();
		$("#addEmpOrg").show();
		$("#listEmpOrg").hide();
		$("#editEmpOrg").hide();
	});
	
	$("#btnListEmpOrg").click(function(){
		$("#addEmpOrg").hide();
		$("#listEmpOrg").show();
		$("#editEmpOrg").hide();
	});
	
});