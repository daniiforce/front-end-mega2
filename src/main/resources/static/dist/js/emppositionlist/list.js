
$( document ).ready(function() {
	$("#listEmpPosition").show();
	var dataEmp = $("#txtEmpCode").val();
	$("#empCodePos").val(dataEmp);
	var isCheck = 0;
	var dataTablesPos = $('#tblEmpPosition').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "bFilter": false,
        "ajax": {
            "url": "./get_list_emp_position",
            "data": function ( d ) {
            	d.empCode = dataEmp,
            	d.isCheck = isCheck
         }},
         /* "columns": [
			{ "data": "", "name" : "pilih" , "title" : ""},
         { "data": "companyCode", "name" : "Company Code" , "title" : "Company Code"},
         { "data": "companyAddress", "name" : "Company Address" , "title" : "Company Address"},
         { "data": "leaderName", "name" : "Leader Name", "title" : "Leader Name"  },
         { "data": "companyName", "name" : "Company Name" , "title" : "Company Name"},
         { "data": "npwp", "name" : "NPWP" , "title" : "NPWP"},
         { "data": "siup", "name" : "SIUP", "title" : "SIUP"  },
         { "data": "isHolding", "name" : "Holding" , "title" : "isHolding"}
     ]    */
	});
	
	
	$('#btnDelEmpPosition').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url:'./delete_emp_pos',
			type:'POST',
			data:{
				dipilih:pilihan
			},
			success: function(){
				dataTablesPos.ajax.reload();	
			}
		});
	});
	
	$('#checkAllEmpPos').click(function(){
		if($(this).is(":checked")){
			$('.chkPosCheckBoxId').prop('checked', true);
			isCheck = 1;
		}else{
			$('.chkPosCheckBoxId').prop('checked', false);
			isCheck = 0;
		}
	});
	
	$("#btnSaveEmpPos").click(function(){
		$.ajax({
			url  : './doAddEmpPosition',
			type : 'POST',
			data : {
				empCode : $("#empCodePos").val(),
				companyId : $("#cmpPosId option:selected").val(),
				posId : $("#posId option:selected").val(),
				validFrom : $('#validFromPos').val(),
				validTo : $('#validToPos').val()
			},
			success: function(){
				dataTablesPos.ajax.reload();
			}
		});
		$("#addEmpPosition").hide();
		$("#listEmpPosition").show();
		$("#editEmpPosition").hide();
	});
	
	
	$('#btnEditEmpPosition').click(function() {
		var pilihan = new Array();
		$.each($("input[id='pilih']:checked"), function(){
			pilihan.push($(this).val());
		});
		$.ajax({
			url 	: './editemppos',
			data	:{
				dipilih:pilihan
			},
			success: function(data) {
				//$("#idEmpPeriodHidden").val(data.id);
				$("#idEmpPos").val(data.id);
				$("#empCodePosEdit").val(data.employeeCode);
				 //$("#cmpIdEdit option[text=" + data.company.companyCode + " : " + data.company.companyName +"]").attr('selected', 'selected');
				$("#cmpIdPosEdit option[value='"+data.companyId+"']").prop('selected', true);
				$("#posIdPosEdit option[value='"+data.positionId+"']").prop('selected', true);
				$('#validFromPosEdit').val(data.strValidFrom);
				$('#validToPosEdit').val(data.strValidTo);
			}
		});
		$("#listEmpPosition").hide();
		$("#addEmpPosition").hide();
		$("#editEmpPosition").show();
	});
	
	$("#btnSaveEmpPosEdit").click(function(){
		$.ajax({
			url		: './doEditEmpPosition',
			type 	: 'POST',
			data	: {
				id : $("#idEmpPos").val(),
				empCode : $("#empCodePosEdit").val(),
				companyId : $("#cmpIdPosEdit option:selected").val(),
				posid : $("#posIdPosEdit option:selected").val(),
				validFrom : $('#validFromPosEdit').val(),
				validTo : $('#validToPosEdit').val()
			},
			success:function(){
				isCheck = 0;
				dataTablesPos.ajax.reload();
			}
		});
		$("#listEmpPosition").show();
		$("#addEmpPosition").hide();
		$("#editEmpPosition").hide();
	});
	
	$("#btnAddEmpPosition").click(function(){
		$("#addEmpPosition").show();
		$("#listEmpPosition").hide();
		$("#editEmpPosition").hide();
	});
	
	$("#btnListEmpPosition").click(function(){
		dataTablesPos.ajax.reload();
		$("#listEmpPosition").show();
		$("#addEmpPosition").hide();
		$("#editEmpPosition").hide();
	});
});